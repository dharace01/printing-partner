package com.printing.partner.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.GestureDetector
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.MotionEvent
import android.view.View
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.printing.partner.R
import com.printing.partner.adapter.BucketsAdapter
import com.printing.partner.base.BaseFragment
import com.printing.partner.ui.OpenGallery
import kotlinx.android.synthetic.main.fragment_gallery.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class GalleryFragment : BaseFragment(R.layout.fragment_gallery) {

    private var mAdapter: BucketsAdapter? = null
    private val projection =
        arrayOf(MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.Images.Media.DATA)
    private val projection2 =
        arrayOf(MediaStore.Images.Media.DISPLAY_NAME, MediaStore.Images.Media.DATA)
    private var bucketNames: List<String> = ArrayList()
    private var bitmapList: List<String> = ArrayList()

    companion object {
        lateinit var imagesList: List<String>
        lateinit var selected: List<Boolean>
    }

    override fun onViewCreated(@NonNull view: View, @Nullable savedInstanceState: Bundle?) {
        bucketNames = ArrayList()
        bitmapList = ArrayList()
        imagesList = ArrayList()
        getPicBuckets()
    }

    private fun getPicBuckets() {
        val cursor = context!!.contentResolver
            .query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection,
                null, null, MediaStore.Images.Media.DATE_ADDED
            )
        val bucketNamesTEMP = java.util.ArrayList<String>(
            cursor!!.count
        )
        val bitmapListTEMP = java.util.ArrayList<String>(
            cursor.count
        )
        val albumSet = HashSet<String>()
        var file: File
        if (cursor.moveToLast()) {
            do {
                if (Thread.interrupted()) {
                    return
                }
                val album = cursor.getString(cursor.getColumnIndex(projection[0]))
                val image = cursor.getString(cursor.getColumnIndex(projection[1]))
                file = File(image)
                if (file.exists() && !albumSet.contains(album)) {
                    bucketNamesTEMP.add(album)
                    bitmapListTEMP.add(image)
                    albumSet.add(album)
                }
            } while (cursor.moveToPrevious())
        }
        cursor.close()

        bucketNames = bucketNamesTEMP
        bitmapList = bitmapListTEMP

        Log.e("list", bucketNames.size.toString())

        populateRecyclerView()
    }

    private fun populateRecyclerView() {
        mAdapter = BucketsAdapter(bucketNames, bitmapList, context)
        val mLayoutManager: RecyclerView.LayoutManager = GridLayoutManager(context, 3)
        recycler_view!!.layoutManager = mLayoutManager
        recycler_view.itemAnimator = DefaultItemAnimator()
        recycler_view.adapter = mAdapter
        recycler_view.addOnItemTouchListener(
            RecyclerTouchListener(
                context,
                recycler_view,
                object :
                    ClickListener {
                    override fun onClick(view: View?, position: Int) {
                        getPictures(bucketNames[position])
                        val intent = Intent(context, OpenGallery::class.java)
                        startActivity(intent)
                    }

                    override fun onLongClick(view: View?, position: Int) {}
                })
        )
        mAdapter!!.notifyDataSetChanged()
    }

    fun getPictures(bucket: String) {
        selected = ArrayList()
        val cursor = context!!.contentResolver
            .query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection2,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME + " =?",
                arrayOf(bucket),
                MediaStore.Images.Media.DATE_ADDED
            )

        val imagesTEMP = java.util.ArrayList<String>(
            cursor!!.count
        )

        val albumSet = HashSet<String>()
        var file: File
        if (cursor.moveToLast()) {
            do {
                if (Thread.interrupted()) {
                    return
                }
                val path = cursor.getString(cursor.getColumnIndex(projection2[1]))
                file = File(path)
                if (file.exists() && !albumSet.contains(path)) {
                    imagesTEMP.add(path)
                    albumSet.add(path)
                    (selected as ArrayList<Boolean>).add(false)
                }
            } while (cursor.moveToPrevious())
        }
        cursor.close()

        imagesList = ArrayList()
        imagesList = imagesTEMP
    }

    interface ClickListener {
        fun onClick(view: View?, position: Int)
        fun onLongClick(view: View?, position: Int)
    }

    class RecyclerTouchListener(
        context: Context?,
        recyclerView: RecyclerView,
        private val clickListener: ClickListener?
    ) : RecyclerView.OnItemTouchListener {
        private val gestureDetector: GestureDetector
        override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
            val child = rv.findChildViewUnder(e.x, e.y)
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child))
            }
            return false
        }

        override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {}

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {}

        init {
            gestureDetector = GestureDetector(context, object : SimpleOnGestureListener() {
                override fun onSingleTapUp(e: MotionEvent): Boolean {
                    return true
                }

                override fun onLongPress(e: MotionEvent) {
                    val child = recyclerView.findChildViewUnder(e.x, e.y)
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child))
                    }
                }
            })
        }
    }

}