package com.printing.partner.base

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.androidnetworking.error.ANError
import com.bumptech.glide.load.HttpException
import com.google.gson.Gson
import com.printing.partner.R
import com.printing.partner.model.ResponseCode
import com.printing.partner.model.api_client.ApiClient
import com.printing.partner.pref.AppPref
import com.printing.partner.ui.SplashScreen
import com.printing.partner.util.CheckInternetConnection
import com.printing.partner.util.HideDialog
import com.printing.partner.util.ShowDialog
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.*
import org.json.JSONObject
import java.net.ConnectException
import java.net.SocketTimeoutException

abstract class BaseFragment(@LayoutRes private val contentLayoutId: Int) :
    Fragment(contentLayoutId) {

    protected val apiClient = ApiClient()
    lateinit var mContext: Context
    lateinit var mActivity: Activity

    var disposable: Disposable? = null

    protected var handler: Handler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
        mContext = context!!
        mActivity = activity!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(contentLayoutId, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity) {
            val activity = context as BaseActivity?
            activity?.let { this.mActivity = it }
        }
    }

    override fun onDetach() {
        super.onDetach()
        if (mContext is BaseActivity) {
            val activity = mContext as BaseActivity?
            activity?.let { this.mActivity = it }
        }
    }

    protected fun <T> callApi(
        observable: Observable<T>,
        showLoader: Boolean = true,
        responseBlock: (T) -> Unit
    ) {

        if (!mContext.CheckInternetConnection()) {
            return
        }

        observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<T> {
                override fun onSubscribe(d: Disposable) {
                    if (showLoader) {
                        mContext.ShowDialog("")
                    }
                }

                override fun onNext(response: T) {
                    Log.e("API", "Success : ${Gson().toJson(response)}")
                    responseBlock(response)
                }

                override fun onError(e: Throwable) {
                    if (showLoader) {
                        mContext.HideDialog()
                    }

                    onResponseFailure(e)
                }

                override fun onComplete() {
                    if (showLoader) {
                        mContext.HideDialog()
                    }
                }
            })
    }

    protected fun <T> callApi(
        observable: Observable<T>,
        swipeRefreshLayout: SwipeRefreshLayout,
        responseBlock: (T) -> Unit
    ) {

        if (!mContext.CheckInternetConnection()) {
            return
        }

        observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<T> {
                override fun onSubscribe(d: Disposable) {
                    swipeRefreshLayout.isRefreshing = true
                }

                override fun onNext(response: T) {
                    Log.e("API", "Success : ${Gson().toJson(response)}")
                    responseBlock(response)
                }

                override fun onError(e: Throwable) {
                    swipeRefreshLayout.isRefreshing = false
                    onResponseFailure(e)
                }

                override fun onComplete() {
                    swipeRefreshLayout.isRefreshing = false
                }
            })
    }

    protected fun <T> callApi(
        observable: Observable<T>,
        swipeRefreshLayout: SwipeRefreshLayout,
        smartRefreshLayout: SmartRefreshLayout,
        responseBlock: (T) -> Unit
    ) {

        if (!mContext.CheckInternetConnection()) {
            return
        }

        observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<T> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(response: T) {
                    Log.e("API", "Success : ${Gson().toJson(response)}")
                    responseBlock(response)
                }

                override fun onError(e: Throwable) {
                    swipeRefreshLayout.isRefreshing = false
                    if (smartRefreshLayout.isLoading) {
                        smartRefreshLayout.finishLoadmore()
                    }

                    onResponseFailure(e)
                }

                override fun onComplete() {
                    swipeRefreshLayout.isRefreshing = false
                    if (smartRefreshLayout.isLoading) {
                        smartRefreshLayout.finishLoadmore()
                    }
                }
            })
    }

    fun onResponseFailure(throwable: Throwable) {
        var error = throwable as (ANError)
        Log.e("API", "Error : ${error.message.toString()}")
        when (throwable) {
            is HttpException -> {
                Log.e("API", "Error : ${error.errorCode}")
                handleResponseError(throwable)
            }
            is ConnectException -> {
                sneakerError(mContext.getString(R.string.msg_no_internet))
            }
            is SocketTimeoutException -> {
                sneakerError(mContext.getString(R.string.time_out))
            }
        }
    }

    private fun handleResponseError(throwable: ANError) {
        when (throwable.errorCode) {
            ResponseCode.InValidateData.code -> {
                val errorRawData = throwable.errorBody
                if (!errorRawData.isNullOrEmpty()) {
                    val jsonObject = JSONObject(errorRawData)
                    val jObject = jsonObject.optJSONObject("errors")
                    if (jObject != null) {
                        val keys: Iterator<String> = jObject.keys()
                        if (keys.hasNext()) {
                            val msg = StringBuilder()
                            while (keys.hasNext()) {
                                val key: String = keys.next()
                                if (jObject.get(key) is String) {
                                    msg.append("- ${jObject.get(key)}\n")
                                }
                            }
                            errorDialog(msg.toString(), getString(R.string.alert))
                        } else {
                            errorDialog(jsonObject.optString("message", ""))
                        }
                    } else {
                        errorDialog(
                            JSONObject(errorRawData).optString("message"),
                            getString(R.string.alert)
                        )
                    }
                }
            }
            ResponseCode.Unauthenticated.code -> {
                val errorRawData = throwable.errorBody
                if (!errorRawData.isNullOrEmpty()) {
                    mActivity.alert(errorRawData, getString(R.string.alert)) {
                        okButton { onAuthFail() }
                    }.show()
                } else {
                    onAuthFail()
                }
            }
            ResponseCode.ForceUpdate.code -> {

            }
            ResponseCode.ServerError.code -> errorDialog(getString(R.string.internal_server_error))
            ResponseCode.BadRequest.code,
            ResponseCode.Unauthorized.code,
            ResponseCode.NotFound.code,
            ResponseCode.RequestTimeOut.code,
            ResponseCode.Conflict.code,
            ResponseCode.Blocked.code -> {
                val errorRawData = throwable.errorBody
                if (!errorRawData.isNullOrEmpty()) {
                    errorDialog(JSONObject(errorRawData).optString("message", ""))
                }
            }
        }
    }

    private fun errorDialog(optString: String?, title: String = getString(R.string.app_name)) {
        optString?.let {
//            sneakerError(it)
            mContext.alert(it, title) { okButton { } }.build().show()
        }
    }

    fun sneakerError(message: String) {
        mContext.toast(message)
    }

    private fun onAuthFail() {
        AppPref.clearPref()
        startActivity(mActivity.intentFor<SplashScreen>().clearTask().newTask())
    }

    open fun hideKeyboardFrom(context: Context, view: View) {
        val imm: InputMethodManager =
            context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}