package com.printing.partner.model.api_client

import android.util.Log
import com.printing.partner.model.*
import com.printing.partner.ui.DashboardActivity
import io.reactivex.Observable
import java.io.File


class ApiClient : ApiMethod {

    var header: HashMap<String, String>? = null

    companion object {
        const val BASE_URL: String = "https://printingpartner.in/printing/"
    }

    constructor()

    private fun getApiHeader(): HashMap<String, String> {
        if (header == null) {
            header = HashMap<String, String>()
            header!!.put("Content-Type", "application/json")
        }
        return header!!
    }

    fun postLogin(request: HashMap<String, String>): Observable<LoginResponse> {
        return RxPostRequest(
            BASE_URL.plus("api_login"),
            request
        ).getObjectObservable(LoginResponse::class.java)
    }

    fun postReference(request: HashMap<String, String>): Observable<ReferenceResponse> {
        return RxPostRequest(
            BASE_URL.plus("api_ref_no"),
            request
        ).getObjectObservable(ReferenceResponse::class.java)
    }

    fun postRegistration(request: HashMap<String, String>): Observable<RegistrationResponse> {
        return RxPostRequest(
            BASE_URL.plus("api_reg"),
            request
        ).getObjectObservable(RegistrationResponse::class.java)
    }

    fun postForgetPasswordVerify(request: HashMap<String, String>): Observable<ForgetPasswordVerifyResponse> {
        return RxPostRequest(
            BASE_URL.plus("forget_password"),
            request
        ).getObjectObservable(ForgetPasswordVerifyResponse::class.java)
    }


    fun postForgetPassword(request: HashMap<String, String>): Observable<ForgetPasswordResponse> {
        return RxPostRequest(
            BASE_URL.plus("update_forget_password"),
            request
        ).getObjectObservable(ForgetPasswordResponse::class.java)
    }

    fun postGetProfile(request: HashMap<String, String>): Observable<ProfileResponse> {
        return RxPostRequest(
            BASE_URL.plus("get_profile"),
            request
        ).getObjectObservable(ProfileResponse::class.java)
    }

    fun postEditProfile(request: HashMap<String, String>): Observable<ProfileResponse> {
        return RxPostRequest(
            BASE_URL.plus("edit_profile"),
            request
        ).getObjectObservable(ProfileResponse::class.java)
    }

    fun getHomeData(): Observable<HomeResponse> {
        return RxGetRequest(
            BASE_URL.plus("api_homedata"),
            HashMap<String, String>(),
        ).getObjectObservable(HomeResponse::class.java)
    }

    fun postAddToCart(request: HashMap<String, String>): Observable<AddToCartResponse> {
        return RxPostRequest(
            BASE_URL.plus("add_to_cart"),
            request
        ).getObjectObservable(AddToCartResponse::class.java)
    }

    fun postUpdateCart(request: HashMap<String, String>): Observable<AddToCartResponse> {
        return RxPostRequest(
            BASE_URL.plus("update_cart"),
            request
        ).getObjectObservable(AddToCartResponse::class.java)
    }

    fun postBulkOrder(request: HashMap<String, String>): Observable<BulkOrderResponse> {
        return RxPostRequest(
            BASE_URL.plus("create_bulk_order"),
            request
        ).getObjectObservable(BulkOrderResponse::class.java)
    }

    fun postImage(
        file: File?,
        s: String,
        userName: String,
        category: String,
        selectedCoverUrl: Int,
    ): Observable<SaveImageResponse> {

        Log.e("type", DashboardActivity.type_name +"---"+ DashboardActivity.subtype_name +"----"
                + DashboardActivity.subsubtype_name)

        return RxPostDynamicRequest1(
            BASE_URL.plus("save_image"),
            file, s, userName, category, selectedCoverUrl
        ).getObjectObservable(SaveImageResponse::class.java)
    }

    fun postOrderPayment(request: HashMap<String, String>): Observable<OrderPaymentResponse> {
        return RxPostRequest(
            BASE_URL.plus("order_payment"),
            request
        ).getObjectObservable(OrderPaymentResponse::class.java)
    }

    fun getCartHistory(request: HashMap<String, String>): Observable<AddToCartResponse> {
        return RxPostRequest(
            BASE_URL.plus("cart_history"),
            request
        ).getObjectObservable(AddToCartResponse::class.java)
    }

    fun getOrderistory(request: HashMap<String, String>): Observable<OrderHistoryResponse> {
        return RxPostRequest(
            BASE_URL.plus("order_history"),
            request
        ).getObjectObservable(OrderHistoryResponse::class.java)
    }


    fun postSendQuery(request: HashMap<String, String>): Observable<OrderPaymentResponse> {
        return RxPostRequest(
            BASE_URL.plus("contact_us"),
            request
        ).getObjectObservable(OrderPaymentResponse::class.java)
    }

    fun postOcassionalDetails(request: HashMap<String, String>): Observable<OcassionalDetailsResponse> {
        return RxPostRequest(
            BASE_URL.plus("api/web_api/occassional_details"),
            request
        ).getObjectObservable(OcassionalDetailsResponse::class.java)
    }

}
