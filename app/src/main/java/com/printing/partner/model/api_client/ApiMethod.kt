package com.printing.partner.model.api_client

import android.util.Log
import com.androidnetworking.common.Priority
import com.printing.partner.ui.DashboardActivity
import com.rx2androidnetworking.Rx2ANRequest
import com.rx2androidnetworking.Rx2AndroidNetworking
import org.json.JSONObject
import java.io.File
import java.util.*


open class ApiMethod {
    //    companion object {
    fun RxPostJsonRequest(
        path: String,
        jsonObject: JSONObject,
        header: HashMap<String, String>
    ): Rx2ANRequest {
        return Rx2AndroidNetworking.post(path)
            .addHeaders(header)
            .addJSONObjectBody(jsonObject)
            .setTag("PostRequest")
            .setPriority(Priority.HIGH)
            .build()
    }

    fun RxPostRequest(path: String): Rx2ANRequest {
        return Rx2AndroidNetworking.post(path)
            .setTag("PostRequest")
            .setPriority(Priority.HIGH)
            .build()
    }

    fun RxPostRequest(path: String, request: HashMap<String, String>): Rx2ANRequest {
        Log.e("Params", request.toString())
        return Rx2AndroidNetworking.post(path) //                .addHeaders(headers)
            .addBodyParameter(request)
            .setTag("PostRequest")
            .setPriority(Priority.HIGH)
            .build()
    }

    fun RxPutRequest(path: String, request: HashMap<String, String>): Rx2ANRequest {
        return Rx2AndroidNetworking.put(path) //                .addHeaders(headers)
            .addBodyParameter(request)
            .setTag("PutRequest")
            .setPriority(Priority.HIGH)
            .build()
    }

    fun RxDeleteRequest(path: String): Rx2ANRequest {
        return Rx2AndroidNetworking.delete(path) //                .addHeaders(headers)
            .setTag("DeleteRequest")
            .setPriority(Priority.HIGH)
            .build()
    }

    fun RxPostAllDynamicRequest(path: String, request: HashMap<Any, Any>): Rx2ANRequest {
        return Rx2AndroidNetworking.post(path)
            .addBodyParameter(request)
            .setTag("PostRequest")
            .setPriority(Priority.HIGH)
            .build()
    }

    fun RxPostDynamicRequest(path: String, request: HashMap<String, Any>): Rx2ANRequest {
        val header = HashMap<String, String>()
        return Rx2AndroidNetworking.post(path) //                .addHeaders(headers)
            .addBodyParameter(request)
            .setTag("PostRequest")
            .setPriority(Priority.HIGH)
            .build()
    }


    fun RxPostDynamicRequest1(
        path: String,
        file: File?,
        s: String,
        userName: String,
        category: String,
        selectedCoverUrl: Int,
    ): Rx2ANRequest {
        val header = HashMap<String, String>()
        header.put("Content-Type", "multipart/form-data")
        if (file != null) {
            if(s == "0"){

                return Rx2AndroidNetworking.upload(path)
                    .addMultipartParameter("type", s, "multipart/form-data")
                    .addMultipartParameter("category", category, "multipart/form-data")
                    .addMultipartParameter("full_name", userName, "multipart/form-data")
                    .addMultipartParameter("type_name",  DashboardActivity.type_name, "multipart/form-data")
                    .addMultipartParameter("stype",  DashboardActivity.subtype_name, "multipart/form-data")
                    .addMultipartParameter("sstype", DashboardActivity.subsubtype_name, "multipart/form-data")
                    .addMultipartFile("image", file)
                    .addHeaders(header)
                    .setTag("PostRequest")
                    .setPriority(Priority.HIGH)
                    .build()

            }else{
                return Rx2AndroidNetworking.upload(path)
                    .addMultipartParameter("type", s, "multipart/form-data")
                    .addMultipartParameter("category", category, "multipart/form-data")
                    .addMultipartParameter("type_name", DashboardActivity.type_name, "multipart/form-data")
                    .addMultipartParameter("full_name", userName, "multipart/form-data")
                    .addMultipartFile("image", file)
                    .addHeaders(header)
                    .setTag("PostRequest")
                    .setPriority(Priority.HIGH)
                    .build()
            }
        } else {

            return Rx2AndroidNetworking.upload(path)
                .addMultipartParameter("type", s, "multipart/form-data")
                .addMultipartParameter("category", category, "multipart/form-data")
                .addMultipartParameter("type_name", DashboardActivity.type_name, "multipart/form-data")
                .addMultipartParameter("full_name", userName, "multipart/form-data")
                .addMultipartParameter("image", selectedCoverUrl.toString(), "multipart/form-data")
                .addHeaders(header)
                .setTag("PostRequest")
                .setPriority(Priority.HIGH)
                .build()




        }
    }

    fun RxPostRequest(
        path: String,
        request: HashMap<String, String>,
        headers: HashMap<String, String>
    ): Rx2ANRequest {
        return Rx2AndroidNetworking.post(path)
            .addHeaders(headers)
            .addBodyParameter(request)
            .setTag("PostRequest")
            .setPriority(Priority.HIGH)
            .build()
    }

    fun RxGetRequest(path: String, body: HashMap<String, String>): Rx2ANRequest {
        return Rx2AndroidNetworking.get(path) //                .addHeaders(headers)
            .addQueryParameter(body)
            .setTag("GetRequest")
            .setPriority(Priority.HIGH)
            .build()
    }

    fun RxGetRequest(
        path: String,
        body: HashMap<String, String>,
        headers: HashMap<String, String>
    ): Rx2ANRequest {
        return Rx2AndroidNetworking.get(path)
            .addHeaders(headers)
            .addQueryParameter(body)
            .setTag("GetRequest")
            .setPriority(Priority.HIGH)
            .build()
    }

    fun RxDownloadRequest(path: String, dirPath: String, fileName: String): Rx2ANRequest {
        return Rx2AndroidNetworking.download(path, dirPath, fileName)
            .setTag("DownloadRequest")
            .setPriority(Priority.HIGH)
            .build()
    }
}
