package com.printing.partner.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

enum class ResponseCode constructor(val code: Int) {
    OK(200),
    BadRequest(400),
    Unauthenticated(401),
    Unauthorized(403),
    NotFound(404),
    RequestTimeOut(408),
    Conflict(409),
    InValidateData(422),
    Blocked(423),
    ForceUpdate(426),
    ServerError(500);
}

@Keep
data class BaseModelResponse<T>(
    var status: Boolean,
    val message: String = "",
    val data: T? = null
) : Serializable

@Keep
data class BaseListResponse<T>(
    var status: Boolean,
    val message: String = "",
    val data: ArrayList<T>? = null
) : Serializable

@Parcelize
data class LoginResponse(
    val status: Boolean? = false,
    val data: LoginRegistrationData? = null,
    val msg: String? = ""
) : Parcelable

@Parcelize
data class RegistrationResponse(
    val status: Boolean? = false,
    val data: LoginRegistrationData? = null,
    val msg: String? = ""
) : Parcelable

@Parcelize
data class ForgetPasswordVerifyResponse(
    val status: Boolean? = false,
    val otp: String? = "",
    val msg: String? = ""
) : Parcelable

@Parcelize
data class ForgetPasswordResponse(
    val status: Boolean? = false,
    val msg: String? = ""
) : Parcelable

@Parcelize
data class ProfileResponse(
    val status: Boolean? = false,
    val data: ProfileData? = null,
    val msg: String? = ""
) : Parcelable

@Parcelize
data class HomeResponse(
    val status: Boolean? = false,
    val data: HomeData? = null,
    val msg: String? = ""
) : Parcelable

@Parcelize
data class LoginRegistrationData(
    val unique_id: String? = "",
    val username: String? = "",
    val fname: String? = "",
    val lname: String? = "",
    val email: String? = "",
    val mobile: String? = "",
    val address: String? = "",
    val city: String? = "",
    val pincode: String? = "",
    val link: String? = "",
    val mob_otp: String? = "",
    val email_otp: String? = "",
) : Parcelable

@Parcelize
data class ProfileData(
    val username: String? = "",
    val fname: String? = "",
    val lname: String? = "",
    val email: String? = "",
    val mobile: String? = "",
    val address: String? = "",
    val city: String? = "",
    val pincode: String? = "",
    val link: String? = "",
) : Parcelable

@Parcelize
data class HomeData(
    val slider: ArrayList<SliderData>? = null,
    val snapbook: SnapbookData? = null,
    val polaroids: SnapbookData? = null,
    val frame: SnapbookData? = null,
    val snap: SnapbookData? = null,
) : Parcelable

@Parcelize
data class SliderData(
    val img: String? = ""
) : Parcelable

@Parcelize
data class SnapbookData(
    val slider: ArrayList<SliderData>? = null,
    val description: String? = null,
    val img: String? = null,
    val shipping_charge: Int? = 0,
    val cover: ArrayList<CoverData>? = null,
    val type: ArrayList<TypeData>? = null,
    val count: Int? = null
) : Parcelable

@Parcelize
data class CoverData(
    val id: Int? = 0,
    val img: String? = "",
    val price: Int? = 0,
    var isSelected: Boolean? = false

) : Parcelable

@Parcelize
data class TypeData(
    val img: String? = null,
    val name: String? = "",
    val size: String? = "",
    val price: Int? = null,
    val count: Int? = null,
    val t_id: Int? = 0,
    val st_id: Int? = 0,
    val sst_id: Int? = 0,
    val subtype: ArrayList<TypeData>? = null,
    val subtype1: ArrayList<TypeData>? = null,
) : Parcelable

@Parcelize
data class SelectedImagesData(
    val img: String? = "",
    val isCropped: Boolean? = false
) : Parcelable

@Parcelize
data class ReferenceResponse(
    val success: Boolean? = false,
    val data: ReferenceData? = null,
    val msg: String? = ""
) : Parcelable

@Parcelize
data class ReferenceData(
    val refrence_no: String? = "",
    val fname: String? = "",
    val lname: String? = "",
    val mobile: String? = "",
    val email: String? = "",
    val address_type: String? = "",
    val wing_no: String? = "",
    val house_no: String? = "",
    val society: String? = "",
    val landmark: String? = "",
    val pincode: String? = "",
    val country: String? = "",
    val state: String? = "",
    val city: String? = "",
    val upline_id: String? = "",
    val status: String? = "",
    val created_on: String? = ""
) : Parcelable

@Parcelize
data class AddToCartResponse(
    val status: Boolean? = false,
    val sub_total: String? = "",
    val msg: String? = "",
    val shipping_charge: String? = "",
    val final_total: String? = "",
    val gst_total: String? = "",
    val data: ArrayList<CartData>? = null ,
    val coverpage_price: String? = ""
) : Parcelable

@Parcelize
data class CartData(
    var cart_id: Int = 0,
    val category: String? = "",
    val type: String? = "",
    val sub_type: String? = "",
    val sub_sub_type: String? = "",
    val qty: String? = "",
    var price: Int = 0,
) : Parcelable

@Parcelize
data class SaveImageResponse(
    val status: Boolean? = false,
    val data: SaveImageData? = null,
    val msg: String? = ""
) : Parcelable

@Parcelize
data class SaveImageData(
    val image_id: String? = ""
) : Parcelable

@Parcelize
data class BulkOrderResponse(
    val status: Boolean? = false,
    val msg: String? = "",
    val order_id: String? = ""
) : Parcelable

@Parcelize
data class OrderPaymentResponse(
    val status: Boolean? = false,
    val msg: String? = "",
) : Parcelable

@Parcelize
data class OrderHistoryResponse(
    val status: Boolean? = false,
    val msg: String? = "",
    val data: ArrayList<OrderHistoryData>? = null
) : Parcelable

@Parcelize
data class OrderHistoryData(
    val order_id: String? = "",
    val order_date: String? = "",
    val delivery_date: String? = "",
    val transaction_id: String? = "",
    val status: String? = "",
    val sub_amount: String? = "",
    val shipping_charge: String? = "",
    val coverpage_price: String? = "",
    val final_amount: String? = "",
    val detail: ArrayList<CartData>? = null
) : Parcelable

@Parcelize
data class OcassionalDetailsResponse(
    val status: Boolean? = false,
    val data: OcassionalDetailsData? = null,
    val msg: String? = ""
) : Parcelable

@Parcelize
data class OcassionalDetailsData(
    val id: String? = "",
) : Parcelable