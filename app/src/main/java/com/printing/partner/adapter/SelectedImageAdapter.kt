package com.printing.partner.adapter

import android.annotation.SuppressLint
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.printing.partner.R
import com.printing.partner.base.BaseAdapter
import com.printing.partner.model.SelectedImagesData
import com.printing.partner.ui.DashboardActivity
import com.printing.partner.util.LoadImage
import kotlinx.android.synthetic.main.item_selected_image.view.*

class SelectedImageAdapter(private val size: String?, private val isCropped: Boolean) :

    BaseAdapter<SelectedImagesData>(R.layout.item_selected_image) {

    override fun setClickableView(itemView: View): List<View?> =
        listOf(itemView.layout)

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onBind(
        viewType: Int,
        view: View,
        position: Int,
        item: SelectedImagesData,
        payloads: MutableList<Any>?
    ) {
        view.run {
            mContext.LoadImage(
                "file://${item.img}",
                image
            )

            val height: String = size!!.substringAfter(":")
            val width: String = size.substringBefore(":")

            (layout.layoutParams as ConstraintLayout.LayoutParams).dimensionRatio =
                if (DashboardActivity.selPlan!!.contains("Snaps") && size == "9:12") "12:9"
                else if (DashboardActivity.selPlanType1!!.contains("Landscape Frame")) """$height:$width"""
                else size

            if (isCropped) layout.background = resources.getDrawable(
                if (item.isCropped!!) R.drawable.selected_images_bg else R.drawable.cropped_images_bg
            )
        }
    }
}