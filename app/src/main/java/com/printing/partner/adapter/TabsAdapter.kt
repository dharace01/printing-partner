package com.printing.partner.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.printing.partner.fragments.FacebookFragment
import com.printing.partner.fragments.GalleryFragment
import com.printing.partner.fragments.InstagramFragment

@Suppress("DEPRECATION")
class TabsAdapter(fm: FragmentManager) :
    FragmentStatePagerAdapter(fm) {
    override fun getItem(i: Int): Fragment {
        return when (i) {

            0 -> GalleryFragment()
            1 -> FacebookFragment()
            else -> InstagramFragment()
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Gallery"
            1 -> "Facebook"
            else -> "Instagram"
        }
    }
}