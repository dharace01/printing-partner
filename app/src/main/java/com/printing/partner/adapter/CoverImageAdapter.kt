package com.printing.partner.adapter

import android.annotation.SuppressLint
import android.view.View
import com.printing.partner.R
import com.printing.partner.base.BaseAdapter
import com.printing.partner.model.CoverData
import com.printing.partner.model.api_client.ApiClient
import com.printing.partner.util.LoadImageCenterCrop
import kotlinx.android.synthetic.main.item_cover_image.view.*

class CoverImageAdapter :

    BaseAdapter<CoverData>(R.layout.item_cover_image) {

    override fun setClickableView(itemView: View): List<View?> =
        listOf(itemView.layout)

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onBind(
        viewType: Int,
        view: View,
        position: Int,
        item: CoverData,
        payloads: MutableList<Any>?
    ) {
        view.run {
            imgBorder.background =
                resources.getDrawable(if (item.isSelected!!) R.drawable.cover_images_bg else R.drawable.selected_images_bg)
            mContext.LoadImageCenterCrop(ApiClient.BASE_URL + item.img!!, image)
            price.text = (item.price ?: 0).toString().plus(" ₹")
        }
    }
}