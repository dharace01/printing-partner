package com.printing.partner.adapter

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.printing.partner.R
import com.printing.partner.model.SliderData
import com.printing.partner.model.api_client.ApiClient
import com.printing.partner.util.LoadImageCenterCrop
import kotlinx.android.synthetic.main.slider_image.view.*

class CustomAdapter(
    private val activity: Activity,
    private val imagesArray: ArrayList<SliderData>
) :
    PagerAdapter() {
    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val viewItem: View = activity.layoutInflater.inflate(
            R.layout.slider_image,
            container,
            false
        )
        activity.LoadImageCenterCrop(
            ApiClient.BASE_URL + imagesArray[position].img!!,
            viewItem.imgTop
        )

        (container as ViewPager).addView(viewItem)
        return viewItem
    }

    override fun getCount(): Int {
        return imagesArray.size
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as View)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as View
    }
}