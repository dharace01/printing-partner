package com.printing.partner.adapter

import android.annotation.SuppressLint
import android.view.View
import com.printing.partner.R
import com.printing.partner.base.BaseAdapter
import com.printing.partner.model.CartData
import kotlinx.android.synthetic.main.item_cart.view.*
import kotlinx.android.synthetic.main.item_list.view.*
import kotlinx.android.synthetic.main.item_list.view.tvName

class CheckoutAdapter :
    BaseAdapter<CartData>(R.layout.item_cart) {

    override fun setClickableView(itemView: View): List<View?> = listOf(itemView.ivMinus, itemView.ivPlus, itemView.ivDelete)

    @SuppressLint("SetTextI18n")
    override fun onBind(
        viewType: Int,
        view: View,
        position: Int,
        item: CartData,
        payloads: MutableList<Any>?
    ) {
        view.run {

            var typeName = item.category
            typeName = if(!item.type.isNullOrEmpty()) typeName + " " + item.type else typeName
            typeName = if(!item.sub_type.isNullOrEmpty()) typeName + " " + item.sub_type else typeName
            typeName = if(!item.sub_sub_type.isNullOrEmpty()) typeName + " " + item.sub_sub_type else typeName

            tvName.text = typeName
            tvPrice.text = item.price.toString().plus(" ₹")
            tvCount.text = item.qty
        }
    }
}