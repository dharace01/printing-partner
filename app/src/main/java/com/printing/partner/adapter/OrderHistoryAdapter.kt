package com.printing.partner.adapter

import android.annotation.SuppressLint
import android.view.View
import com.printing.partner.R
import com.printing.partner.base.BaseAdapter
import com.printing.partner.model.CartData
import com.printing.partner.model.OrderHistoryData
import kotlinx.android.synthetic.main.item_cart.view.*
import kotlinx.android.synthetic.main.item_list.view.*
import kotlinx.android.synthetic.main.item_list.view.tvName
import kotlinx.android.synthetic.main.item_order_history.view.*

class OrderHistoryAdapter :
    BaseAdapter<OrderHistoryData>(R.layout.item_order_history) {

    override fun setClickableView(itemView: View): List<View?> = listOf(itemView.btnOrderDetails)

    override fun onBind(
        viewType: Int,
        view: View,
        position: Int,
        item: OrderHistoryData,
        payloads: MutableList<Any>?
    ) {
        view.run {
            tvOrderId.text =  "Order ID - " + item.order_id
            tvOrderTime.text =  "Order Date - " + item.order_date
            tvOrderExpectedTime.text =  "Expected Delivery Date - " + item.delivery_date
            btnOrderStatus.text = item.status
        }
    }
}