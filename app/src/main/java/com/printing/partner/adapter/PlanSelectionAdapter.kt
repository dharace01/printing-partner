package com.printing.partner.adapter

import android.view.View
import com.printing.partner.R
import com.printing.partner.base.BaseAdapter
import com.printing.partner.model.TypeData
import com.printing.partner.model.api_client.ApiClient
import com.printing.partner.util.LoadImage
import kotlinx.android.synthetic.main.item_list.view.*

class PlanSelectionAdapter :
    BaseAdapter<TypeData>(R.layout.item_list) {

    override fun setClickableView(itemView: View): List<View?> =
        listOf(itemView.btnSelect, itemView.tvName, itemView.layout)

    override fun onBind(
        viewType: Int,
        view: View,
        position: Int,
        item: TypeData,
        payloads: MutableList<Any>?
    ) {
        view.run {
            mContext.LoadImage(
                ApiClient.BASE_URL + item.img,
                ivLogo
            )
            tvName.text = (item.name ?: 0).toString()
            tvSize.text = (item.size ?: 0).toString()
            if (item.size.equals("")) {
                tvSize.visibility = View.GONE
            }
            if (item.price != null && item.price != 0) {
                btnSelect.text = item.price.toString().plus(" ₹")
            }
        }
    }
}