package com.printing.partner.pref

import com.chibatching.kotpref.KotprefModel
import com.chibatching.kotpref.gsonpref.gsonNullablePref

object AppPref : KotprefModel() {

    fun clearPref() {
        clear()
    }

    var IsLogin by booleanPref(false)
    var IsGuest by booleanPref(false)
    var userID by stringPref()
    var userName by stringPref()
//    var getLoginData by gsonNullablePref<LoginData>()
//    var getUserById by gsonNullablePref<UserByIdResponse>()

    var isLoginUpdate by booleanPref(false)
    var isLocationSuccess by booleanPref(false)
    var authToken by stringPref("abc")
    var share_code by stringPref()
    var age by stringPref()
    var appLocaleCode by stringPref("en")
    var isLangSelected by booleanPref()
    var isCompletedSlide by booleanPref()
    var latitude by stringPref("0.0")
    var longitude by stringPref("0.0")
    var recent by gsonNullablePref<ArrayList<String>>(ArrayList())

}