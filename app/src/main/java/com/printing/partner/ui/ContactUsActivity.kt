package com.printing.partner.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.printing.partner.R
import com.printing.partner.base.BaseActivity
import kotlinx.android.synthetic.main.activity_contact_us.*
import kotlinx.android.synthetic.main.activity_order_history.ivBack

class ContactUsActivity : BaseActivity(R.layout.activity_contact_us), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initOnClick()
    }

    private fun initOnClick() {
        ivBack.setOnClickListener(this)
        tvTerms.setOnClickListener(this)
        tvPrivacy.setOnClickListener(this)
        btnQuery.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> onBackPressed()
            R.id.btnQuery -> startActivity(Intent(this, SendQueryActivity::class.java))
            R.id.tvTerms -> startActivity(Intent(this, FaqActivity::class.java).putExtra("url", "http://printingpartner.in/ter_con.html"))
            R.id.tvPrivacy -> startActivity(Intent(this, FaqActivity::class.java).putExtra("url", "http://printingpartner.in/pri_po.html"))
        }
    }
}