package com.printing.partner.ui

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.printing.partner.R
import com.printing.partner.base.BaseActivity
import com.printing.partner.model.LoginResponse
import com.printing.partner.pref.AppPref
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask
import kotlin.toString as toString1

class LoginActivity : BaseActivity(R.layout.activity_login), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initOnClick()
    }

    private fun initOnClick() {
        tvSignup.setOnClickListener(this)
        tvForgotPass.setOnClickListener(this)
        btnSignIn.setOnClickListener(this)
        ivBack.setOnClickListener(this)
        tvGuest.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> onBackPressed()
            R.id.tvSignup -> startActivity(Intent(this, RegisterActivity::class.java))
            R.id.btnSignIn -> if (isValidate()) setLogin()
            R.id.tvForgotPass -> startActivity(Intent(this, ForgetPasswordActivity::class.java))
            R.id.tvGuest -> {
                startActivity(intentFor<GuestActivity>().newTask())
                finish()
            }
        }
    }

    private fun isValidate(): Boolean {
        return when {
            TextUtils.isEmpty(edUsername.text.toString().trim()) -> {
                edUsername.error = getString(R.string.please_enter_username)
                false
            }
            TextUtils.isEmpty(txtPassword.text.toString().trim()) -> {
                txtPassword.error = getString(R.string.please_enter_your_password)
                false
            }
            else -> {
                true
            }
        }
    }

    private fun setLogin() {
        val prm = HashMap<String, String>()
        prm["username"] = edUsername.text.toString().trim()
        prm["password"] = txtPassword.text.toString().trim()
        callApi(apiClient.postLogin(prm), true) { loginResponse: LoginResponse ->
            if (loginResponse.status!!) {
                AppPref.IsLogin = true
                AppPref.userID = loginResponse.data!!.unique_id.toString1()
                AppPref.userName = """${loginResponse.data.fname} ${loginResponse.data.lname}"""
                //   startActivity(Intent(this, MyAccountActivity::class.java))
                finish()
            } else {
                sneakerError(loginResponse.msg!!)
            }
        }
    }
}