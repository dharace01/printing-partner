package com.printing.partner.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.GravityCompat
import com.printing.partner.R
import com.printing.partner.base.BaseActivity
import com.printing.partner.model.SnapbookData
import com.printing.partner.pref.AppPref
import com.printing.partner.util.setUpSlider
import kotlinx.android.synthetic.main.activity_dashboard.*
import java.io.File

class DashboardActivity : BaseActivity(R.layout.activity_dashboard), View.OnClickListener {

    private var snapbookData: SnapbookData? = null
    private var polaroidsData: SnapbookData? = null
    private var frameData: SnapbookData? = null
    private var snapsData: SnapbookData? = null

    private var snapbook: String = "Snapbook"
    private var polaroids: String = "Polaroids"
    private var frame: String = "Frame"
    private var snaps: String = "Snaps"

    companion object {

        @JvmField
        var selPlan: String? = ""

        @JvmField
        var selPlanType: String? = ""

        @JvmField
        var selPlanType1: String? = ""

        @JvmField
        var selPlanType2: String? = ""

        @JvmField
        var selData: SnapbookData? = null

        @JvmField
        var maxCount: Int? = null

        @JvmField
        var selPrice: Int = 0

        @JvmField
        var selCoverPrice: Int = 0

        @JvmField
        var shipping_charge: Int = 0

        @JvmField
        var selSize: String? = ""

        @JvmField
        var type_id: Int = 0

        @JvmField
        var type_name: String = ""

        @JvmField
        var subtype_id: Int = 0

        @JvmField
        var subtype_name: String = ""

        @JvmField
        var subsubtype_id: Int = 0

        @JvmField
        var subsubtype_name: String = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
        deleteCache(this)
    }

    override fun onStart() {
        super.onStart()
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
    }

    private fun initView() {
        callGetBanners()
    }

    private fun initOnClick() {
        ivMenu.setOnClickListener(this)
        tvAccount.setOnClickListener(this)
        ivSnapbook.setOnClickListener(this)
        ivPolaroids.setOnClickListener(this)
        ivFrame.setOnClickListener(this)
        ivSnaps.setOnClickListener(this)
        tvFaq.setOnClickListener(this)
        myCart.setOnClickListener(this)
        myOrders.setOnClickListener(this)
        contactUs.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivMenu -> if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                drawer_layout.closeDrawer(GravityCompat.START)
            } else {
                drawer_layout.openDrawer(GravityCompat.START)
            }
            R.id.tvAccount -> if (AppPref.IsLogin) startActivity(
                Intent(
                    this,
                    MyAccountActivity::class.java
                )
            ) else startActivity(Intent(this, LoginActivity::class.java))
            R.id.myCart -> startActivity(Intent(this,
                CheckoutActivity::class.java).putExtra("isCart",
                true))
            R.id.contactUs -> startActivity(Intent(this, ContactUsActivity::class.java))
            R.id.myOrders -> startActivity(Intent(this,
                if (AppPref.IsLogin) OrderHistoryActivity::class.java else LoginActivity::class.java))
            R.id.ivSnapbook -> goToNextScreen(snapbook, snapbookData)
            R.id.ivPolaroids -> goToNextScreen(polaroids, polaroidsData)
            R.id.ivFrame -> goToNextScreen(frame, frameData)
            R.id.ivSnaps -> goToNextScreen(snaps, snapsData)
            R.id.tvFaq -> startActivity(Intent(this, FaqActivity::class.java).putExtra("url",
                "https://printingpartner.in/faq.html"))
        }
    }

    private fun goToNextScreen(title: String, data: SnapbookData?) {
        PhotosSelectionActivity.selectedImages = ArrayList()
        PhotosSelectionActivity.selectedCover = ArrayList()
        maxCount = null
        selPlanType = ""
        selPlanType1 = ""
        selPlanType2 = ""
        selPlan = title
        selData = data
        type_id = 0
        subsubtype_id = 0
        subtype_id = 0

        type_name = ""
        subtype_name = ""
        subsubtype_name = ""

        if (data != null) {
            startActivity(Intent(this, PlanSelectionActivity::class.java)
                .putExtra("list", data.type)
                .putExtra("title", selPlan)
            )
        }
    }

    private fun callGetBanners() {
        callApi(apiClient.getHomeData(), true) { homeResponse ->
            setUpSlider(homeResponse.data?.slider, viewPager)
            snapbookData = homeResponse.data?.snapbook
            polaroidsData = homeResponse.data?.polaroids
            frameData = homeResponse.data?.frame
            snapsData = homeResponse.data?.snap
        }
    }

    private fun deleteCache(context: Context)
    {
        try {
            val dir: File = context.cacheDir
            deleteDir(dir)
        } catch (e: java.lang.Exception) {
        }
    }
    private fun deleteDir(dir: File): Boolean {
        return when {
            dir.isDirectory -> {
                val children = dir.list()
                for (i in children.indices) {
                    val success = deleteDir(File(dir, children[i]))
                    if (!success) {
                        return false
                    }
                }
                dir.delete()
            }
            dir.isFile -> {
                dir.delete()
            }
            else -> {
                false
            }
        }
    }

    override fun onBackPressed() = if (drawer_layout.isDrawerOpen(GravityCompat.START)) drawer_layout.closeDrawer(
        GravityCompat.START) else super.onBackPressed()
}