package com.printing.partner.ui

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.printing.partner.R
import com.printing.partner.base.BaseActivity
import com.printing.partner.pref.AppPref
import com.printing.partner.util.isValidEmail
import com.printing.partner.util.isValidPhoneNumber
import kotlinx.android.synthetic.main.activity_guest.*
import kotlinx.android.synthetic.main.activity_login.edUsername
import kotlinx.android.synthetic.main.activity_login.txtPassword
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.edCity
import kotlinx.android.synthetic.main.activity_register.edCode
import kotlinx.android.synthetic.main.activity_register.edEmail
import kotlinx.android.synthetic.main.activity_register.edFname
import kotlinx.android.synthetic.main.activity_register.edLname
import kotlinx.android.synthetic.main.activity_register.edMobile
import kotlinx.android.synthetic.main.activity_register.edShippingAdd
import kotlinx.android.synthetic.main.activity_register.ivBack

class RegisterActivity : BaseActivity(R.layout.activity_register), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
    }

    private fun initOnClick() {
        btnSignUp.setOnClickListener(this)
        ivBack.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> {
                onBackPressed()
            }
            R.id.btnSignUp -> {
                if (isValidate()) {
                    setRegister()
                }
            }
        }
    }

    private fun isValidate(): Boolean {
        return when {
            TextUtils.isEmpty(edUsername.text.toString().trim()) -> {
                edUsername.error = getString(R.string.please_enter_username)
                false
            }
            edUsername.text.toString().trim().length < 5 -> {
                edUsername.error = "Username must be atleast 5 letters!"
                false
            }
            TextUtils.isEmpty(txtPassword.text.toString().trim()) -> {
                txtPassword.error = getString(R.string.please_enter_your_password)
                false
            }
            TextUtils.isEmpty(txtConfirmPassword.text.toString().trim()) -> {
                txtConfirmPassword.error = getString(R.string.please_enter_confirm_password)
                false
            }
            txtPassword.text.toString() != txtConfirmPassword.text.toString() -> {
                txtConfirmPassword.error = getString(R.string.password_confirm_password_mismatch)
                false
            }
            TextUtils.isEmpty(edFname.text.toString().trim()) -> {
                edFname.error = getString(R.string.please_enter_your_first_name)
                false
            }
            TextUtils.isEmpty(edLname.text.toString().trim()) -> {
                edLname.error = getString(R.string.please_enter_your_last_name)
                false
            }
            !isValidPhoneNumber(edMobile.text.toString().trim()) -> {
                edMobile.error = getString(R.string.please_enter_your_mobile_number)
                false
            }
            !isValidEmail(edEmail.text.toString().trim()) -> {
                edEmail.error = getString(R.string.please_enter_valid_email_address)
                false
            }
            TextUtils.isEmpty(edShippingAdd.text.toString().trim()) -> {
                edShippingAdd.error = getString(R.string.please_enter_your_shipping_address)
                false
            }
            TextUtils.isEmpty(edCity.text.toString().trim()) -> {
                edCity.error = getString(R.string.please_enter_your_city)
                false
            }
            TextUtils.isEmpty(edCode.text.toString().trim()) -> {
                edCode.error = getString(R.string.please_enter_your_pincode)
                false
            }
           // !(chkMobile.isChecked ||
            !chkEmail.isChecked -> {
                sneakerError("Please add Email verification")
                false
            }
            else -> {
                true
            }
        }
    }

    private fun setRegister() {

        val prm = HashMap<String, String>()
        prm["fname"] = edFname.text.toString().trim()
        prm["lname"] = edLname.text.toString().trim()
        prm["mobile"] = edMobile.text.toString().trim()
        prm["email"] = edEmail.text.toString().trim()
        prm["password"] = txtPassword.text.toString().trim()
        prm["address"] = edShippingAdd.text.toString().trim()
        prm["city"] = edCity.text.toString().trim()
        prm["pincode"] = edCode.text.toString().trim()
        prm["username"] = edUsername.text.toString().trim()
        prm["mob_check"] = if (chkMobile.isChecked) "1" else "0"
        prm["email_check"] = if (chkEmail.isChecked) "1" else "0"
        prm["is_register"] = "0"
        prm["is_guest"] = "0"
        prm["billing_add"] = ""
        prm["billing_city"] =  ""
        prm["billing_pincode"] = ""

        callApi(apiClient.postRegistration(prm), true) { registrationResponse ->
            if (registrationResponse.status!!) {
                startActivity(Intent(this, VerificationActivity::class.java)
                    .putExtra("mob_otp", registrationResponse.data!!.mob_otp)
                    .putExtra("email_otp", registrationResponse.data.email_otp)
                    .putExtra("fname", edFname.text.toString().trim())
                    .putExtra("lname", edLname.text.toString().trim())
                    .putExtra("mobile", edMobile.text.toString().trim())
                    .putExtra("email", edEmail.text.toString().trim())
                    .putExtra("password", txtPassword.text.toString().trim())
                    .putExtra("address", edShippingAdd.text.toString().trim())
                    .putExtra("city", edCity.text.toString().trim())
                    .putExtra("pincode", edCode.text.toString().trim())
                    .putExtra("username", edUsername.text.toString().trim())
                )
                finish()
            } else {
                sneakerError(registrationResponse.msg!!)
            }
        }
    }
}