package com.printing.partner.ui

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.printing.partner.R
import com.printing.partner.adapter.OrderDetailsAdapter
import com.printing.partner.base.BaseActivity
import com.printing.partner.model.CartData
import com.printing.partner.model.OrderHistoryData
import kotlinx.android.synthetic.main.activity_order_details.*

class OrderDetailsActivity : BaseActivity(R.layout.activity_order_details), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        val data = intent.getParcelableExtra<OrderHistoryData>("data")
        if (data != null) {
            tvTitle.text = data.order_id
            setDataInView(data.sub_amount, data.shipping_charge, data.final_amount, data.detail, data.coverpage_price)
        }
    }

    private fun initOnClick() {
        ivBack.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> onBackPressed()
        }
    }

    private fun setDataInView(
        subTotal: String?,
        shippingCharge: String?,
        finalTotal: String?,
        cartList: ArrayList<CartData>?,
        coverpagePrice: String?,
    ) {
        tvPrice.text = subTotal.toString().plus(" ₹")
        tvShippingPrice.text = shippingCharge.toString().plus(" ₹")
        tvTotalPrice.text = finalTotal.toString().plus(" ₹")
        tvCoverPrice.text = if(coverpagePrice!!.isEmpty()) "0 ₹" else coverpagePrice.toString().plus(" ₹")
        rvCart.layoutManager = LinearLayoutManager(this)

        rvCart.run {
            adapter = OrderDetailsAdapter().also {
                if (cartList != null) it.addAll(cartList) else ArrayList<CartData>();
            }
        }
    }
}

