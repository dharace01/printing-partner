package com.printing.partner.ui

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.printing.partner.R
import com.printing.partner.base.BaseActivity
import com.printing.partner.model.LoginResponse
import com.printing.partner.pref.AppPref
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.activity_details.ivBack
import kotlinx.android.synthetic.main.activity_register.*
import java.text.SimpleDateFormat
import java.util.*

class DetailsActivity : BaseActivity(R.layout.activity_details), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {}

    private fun initOnClick() {
        ivBack.setOnClickListener(this)
        btnDone.setOnClickListener(this)
        edDOB.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> {
                onBackPressed()
            }
            R.id.btnDone -> {
                PhotosSelectionActivity.selectedCoverUrl = 0
                if (AppPref.IsLogin || AppPref.IsGuest) {
                    callOcassionalDetailsAPI()
                } else {
                    sneakerError("Please login first to continue!")
                    startActivity(Intent(this, LoginActivity::class.java))
                }
            }
            R.id.edDOB -> {
                val c = Calendar.getInstance()
                val year = c.get(Calendar.YEAR)
                val month = c.get(Calendar.MONTH)
                val day = c.get(Calendar.DAY_OF_MONTH)
                val dpd = DatePickerDialog(this, { _, year, monthOfYear, dayOfMonth ->
                    c.set(Calendar.YEAR, year)
                    c.set(Calendar.MONTH, monthOfYear)
                    c.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    val myFormat = "dd/MM/yyyy" // mention the format you need
                    val sdf = SimpleDateFormat(myFormat, Locale.US)
                    edDOB.setText(sdf.format(c.time).toString())
                }, year, month, day)
                dpd.show()
            }
        }
    }

    private fun callOcassionalDetailsAPI() {
        val prm = HashMap<String, String>()
        prm["name"] = edPName.text.toString().trim()
        prm["occassion"] = edOcassionName.text.toString().trim()
        prm["dob"] = edDOB.text.toString().trim()
        prm["hails_from"] = edHails.text.toString().trim()
        prm["loves_eating"] = edEat.text.toString().trim()
        prm["mostly_called"] = edNickname.text.toString().trim()
        prm["says"] = edSays.text.toString().trim()
        prm["strength"] = edStrength.text.toString().trim()
        prm["abs_love"] = edLoves.text.toString().trim()
        prm["fav_adda"] = edFavAdda.text.toString().trim()
        prm["fav_game"] = edFavGame.text.toString().trim()
        prm["also"] = edAlso.text.toString().trim()
        prm["unique_id"] = AppPref.userID
        callApi(apiClient.postOcassionalDetails(prm), true) {
            if (it.status!!) {
                AppPref.IsGuest = false
                startActivity(Intent(this, CheckoutActivity::class.java)
                    .putExtra("isCart", false)
                    .putExtra("occ_id", it.data!!.id))
               // finish()
            } else {
                sneakerError(it.msg!!)
            }
        }
    }
}