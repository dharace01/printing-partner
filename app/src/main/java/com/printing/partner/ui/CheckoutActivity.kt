package com.printing.partner.ui

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.printing.partner.R
import com.printing.partner.adapter.CheckoutAdapter
import com.printing.partner.base.BaseActivity
import com.printing.partner.model.CartData
import com.printing.partner.pref.AppPref
import com.printing.partner.util.HideDialog
import com.printing.partner.util.ShowDialog
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import kotlinx.android.synthetic.main.activity_checkout.*
import kotlinx.android.synthetic.main.activity_photos_selection.ivBack
import org.json.JSONObject
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class CheckoutActivity : BaseActivity(R.layout.activity_checkout),
    View.OnClickListener, PaymentResultListener {

    val TAG: String = PaymentActivity::class.toString()
    var total: String = "0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {

        llMain.visibility = View.GONE

        if(intent.getBooleanExtra("isCart", true)){
            callGetCart()
            tvTitle.text = "My Cart"
        }else{
            tvTitle.text = "Place Order"
            callAddToCart()
            Log.e(
                "plan", "selPlan :" + DashboardActivity.selPlan + "\nselPlanType: " +
                        DashboardActivity.selPlanType + "\nselPrice :" +
                        DashboardActivity.selPrice + "\nselCoverPrice: " +
                        DashboardActivity.selCoverPrice + "\nselSize :" +
                        DashboardActivity.selSize + "\n maxCount :" +
                        DashboardActivity.maxCount + "\n selData :" +
                        DashboardActivity.selData!!.shipping_charge + "\n" +
                        PhotosSelectionActivity.selectedImages + "\n" +
                        PhotosSelectionActivity.selectedCover + "\n" +
                        "typeid : " + DashboardActivity.type_id
            )
        }
    }

    private fun callGetCart() {
        val prm = HashMap<String, String>()
        prm["unique_id"] = AppPref.userID
        callApi(apiClient.getCartHistory(prm), true) { (status, sub_total, msg, shipping_charge, final_total, gst_total, data, coverpage_price: String?) ->
            HideDialog()
            setDataInView(status,
                sub_total,
                msg,
                shipping_charge,
                final_total,
                gst_total,
                data,
                coverpage_price)
        }
    }

    private fun initOnClick() {
        ivBack.setOnClickListener(this)
        btnCheckout.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> onBackPressed()
            R.id.btnCheckout ->  callBulkOrder()//startPayment()
        }
    }

    var orderId = ""
    private fun callBulkOrder() {
        if(cartList!=null && cartList!!.size == 0){
           finish()
        }else{
            var cartId = ""
            for (cartData in cartList!!) {
                cartId = cartId.plus(cartData.cart_id.toString()).plus(",")
            }

            val prm = HashMap<String, String>()
            prm["unique_id"] = AppPref.userID
            prm["cart_id"] = cartId
            callApi(apiClient.postBulkOrder(prm), true) {
                orderId = it.order_id.toString()
                startPayment()
            }
        }
    }

    var images = "";
    var coverpics = "";
    var category = "";
    var x = 0;
    private fun callAddToCart() {
        coverpics = ""
        images = ""

        if (DashboardActivity.selPlan == "Snapbook") category = "snapbook"
        if (DashboardActivity.selPlan == "Polaroids") category = "polaroids"
        if (DashboardActivity.selPlan == "Frame") category = "frames"
        if (DashboardActivity.selPlan == "Snaps") category = "snap"

        Log.e("array", SelectedImagesActivity.finalImages.toString())
        x= 0;
        if (x < SelectedImagesActivity.finalImages.size) {
            ShowDialog("Uploading Images to Cart " + 1 +"%")
            callUploadImage()
        }

        if (DashboardActivity.selPlan == "Snapbook") {
            if (PhotosSelectionActivity.selectedCoverUrl != 0) {
                callApi(
                    apiClient.postImage(null, "2", AppPref.userName , DashboardActivity.selPlan.toString(), PhotosSelectionActivity.selectedCoverUrl), false
                ) {
                    if (it.status!!) {
                        coverpics = coverpics.plus(it.data?.image_id.toString())
                    } else {
                        sneakerError("Error while add to cart!")
                    }
                    Log.e("Cart", it.toString())
                }
            } else {
                for (file in PhotosSelectionActivity.selectedCover) {
                    callApi(apiClient.postImage(File(file.img), "1", AppPref.userName , DashboardActivity.selPlan.toString(),0), false) {
                        if (it.status!!) {
                            coverpics = coverpics.plus(it.data?.image_id.toString()).plus(",")
                        } else {
                            sneakerError("Error while add to cart!")
                        }
                        Log.e("Cart", it.toString())
                    }
                }
            }
        }
    }

    private fun callUploadImage() {
        callApi(apiClient.postImage(SelectedImagesActivity.finalImages[x], "0",AppPref.userName , DashboardActivity.selPlan.toString(), PhotosSelectionActivity.selectedCoverUrl), false
        ) {
           // HideDialog()
            if (it.status!!) images = images.plus(it.data?.image_id.toString()).plus(",")
            else sneakerError("Error while add to cart!")
            x++
            if (x < SelectedImagesActivity.finalImages.size) {
                HideDialog()
                ShowDialog("Uploading Images to Cart " + (100 - 100/x+2) +"%")
                callUploadImage()
            }
            Log.e("Cart", x.toString() + "---"+SelectedImagesActivity.finalImages.size)
            if (x == SelectedImagesActivity.finalImages.size) {
                callCartAPI()
            }
        }
    }

    private fun callCartAPI() {
        val prm = HashMap<String, String>()
        prm["unique_id"] = AppPref.userID
        prm["type_id"] = DashboardActivity.type_id.toString()
        prm["subtype_id"] = DashboardActivity.subtype_id.toString()
        prm["sub_subtype_id"] = DashboardActivity.subsubtype_id.toString()
        prm["price"] = DashboardActivity.selPrice.toString()
        prm["shipping_charge"] = DashboardActivity.selData!!.shipping_charge!!.toString()
        prm["category"] = DashboardActivity.selPlan.toString()
        prm["img_id"] = images
        prm["cp_id"] = coverpics
        prm["coverpage_price"] = DashboardActivity.selCoverPrice.toString()
        prm["qty"] = "1"
        prm["occ_id"] = intent.getStringExtra("occ_id").toString()

        callApi(apiClient.postAddToCart(prm), false) { (status, sub_total, msg, shipping_charge, final_total, gst_total, data, coverpage_price: String?) ->
            HideDialog()
            setDataInView(status, sub_total, msg, shipping_charge, final_total, gst_total, data, coverpage_price)
        }
    }

    private fun callUpdateAPI(cartId: Int, qty: Int?) {
        val prm = HashMap<String, String>()
        prm["unique_id"] = AppPref.userID
        prm["cart_id"] = cartId.toString()
        prm["qty"] = qty.toString()
        callApi(
            apiClient.postUpdateCart(prm),
            true
        ) { (status: Boolean?, sub_total: String?, msg: String?, shipping_charge: String?, final_total: String?,  gst_total, data: ArrayList<CartData>?,  coverpage_price: String?) ->
            setDataInView(status, sub_total, msg, shipping_charge, final_total,   gst_total,data, coverpage_price)
        }
    }

    var cartList: ArrayList<CartData>? = ArrayList()
    private fun setDataInView(
        status: Boolean?,
        subTotal: String?,
        msg: String?,
        shippingCharge: String?,
        finalTotal: String?,
        gst_total: String?,
        data: ArrayList<CartData>?,
        coverpage_price: String?
    ) {
        if (status!!) {
            tvPrice.text = subTotal.toString().plus(" ₹")
            tvShippingPrice.text = shippingCharge.toString().plus(" ₹")
            tvTotalPrice.text = finalTotal.toString().plus(" ₹")
            tvGst.text =   gst_total.toString().plus(" ₹")
            tvCoverPrice.text = coverpage_price.toString().plus(" ₹")
            rvCart.layoutManager = LinearLayoutManager(this)
            cartList = data
            total = finalTotal.toString();
            if(cartList!=null && cartList!!.size >0){
                llMain.visibility = View.VISIBLE
            }else{
                llMain.visibility = View.GONE
            }

            rvCart.run {
                adapter = CheckoutAdapter().also {
                    if (cartList != null) it.addAll(cartList) else ArrayList<CartData>();
                    it.setItemClickListener { view, _, typeData ->
                        when (view.id) {
                            R.id.ivMinus -> callUpdateAPI(typeData.cart_id, typeData.qty!!.toInt() -1)
                            R.id.ivPlus -> callUpdateAPI(typeData.cart_id, typeData.qty!!.toInt() + 1)
                            R.id.ivDelete -> callUpdateAPI(typeData.cart_id, 0)
                        }
                    }
                }
            }
        }else{
            llMain.visibility = View.GONE
        }
    }

    private fun startPayment() {
        val activity: Activity = this
        val co = Checkout()
        try {
            val options = JSONObject()
            options.put("name", "Razorpay Corp")
            options.put("description", "Demoing Charges")
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png")
            options.put("currency", "INR")
            options.put("amount", Integer.parseInt(total)*100)

//            val prefill = JSONObject()
//            prefill.put("email", "test@razorpay.com")
//            prefill.put("contact", "9876543210")
//            options.put("prefill", prefill)

            co.open(activity, options)
        } catch (e: Exception) {
            Toast.makeText(activity, "Error in payment: " + e.message, Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }
    }

    override fun onPaymentError(errorCode: Int, response: String?) {
        try {
            Toast.makeText(this, "Payment failed $errorCode \n $response", Toast.LENGTH_LONG).show()
        } catch (e: Exception) {
            Log.e(TAG, "Exception in onPaymentSuccess", e)
        }
    }

    override fun onPaymentSuccess(razorpayPaymentId: String?) {
        try {
            callOrdeerPaymentAPI(razorpayPaymentId)
        } catch (e: Exception) {
            Log.e(TAG, "Exception in onPaymentSuccess", e)
        }
    }

    private fun callOrdeerPaymentAPI(razorpayPaymentId: String?) {
        val prm = HashMap<String, String>()
        prm["order_id"] = orderId
        prm["transaction_id"] =razorpayPaymentId.toString()
        callApi(
            apiClient.postOrderPayment(prm),
            true
        ) { (status, msg) ->
           if(status!!){
               Toast.makeText(this, "Payment Successful!", Toast.LENGTH_LONG).show()
               finish()
           }else{
               sneakerError(msg!!)
           }
        }
    }
}