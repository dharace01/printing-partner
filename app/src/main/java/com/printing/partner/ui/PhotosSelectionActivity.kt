package com.printing.partner.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.printing.partner.R
import com.printing.partner.adapter.TabsAdapter
import com.printing.partner.base.BaseActivity
import com.printing.partner.model.SelectedImagesData
import com.printing.partner.util.ShowAlertDialog
import kotlinx.android.synthetic.main.activity_photos_selection.*


class PhotosSelectionActivity : BaseActivity(R.layout.activity_photos_selection),
    View.OnClickListener {

    companion object {
        @JvmField
        var maxCount: Int = 0

        @JvmField
        var title: String = ""

        @JvmField
        var selectedImages = ArrayList<SelectedImagesData>()

        @JvmField
        var selectedCover = ArrayList<SelectedImagesData>()

        @JvmField
        var selectedCoverUrl : Int = 0

        @JvmField
        var selectedCoverType : Int = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
        Log.e("Cover", intent.getBooleanExtra("isCover", false).toString());
    }

    private fun initView() {

        pager.adapter = TabsAdapter(supportFragmentManager)
        tab_layout.setupWithViewPager(pager)
        pager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int,
            ) {
            }

            override fun onPageSelected(position: Int) {
                OpenGallery.selected = ArrayList()
                OpenGallery.imagesSelected = ArrayList()

                maxCount = intent.getIntExtra("count", 0)
                PhotosSelectionActivity.title = "${OpenGallery.imagesSelected.size}/$maxCount"
                tvTitle.text = PhotosSelectionActivity.title
            }
        })
    }

    override fun onResume() {
        super.onResume()
        maxCount = intent.getIntExtra("count", 0)
        PhotosSelectionActivity.title = "${OpenGallery.imagesSelected.size}/$maxCount"
        tvTitle.text = PhotosSelectionActivity.title
    }

    private fun initOnClick() {
        ivBack.setOnClickListener(this)
        tvNext.setOnClickListener(this)
    }

    private var isNext = false
    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> onBackPressed()
            R.id.tvNext -> if (OpenGallery.imagesSelected.size <= 0) ShowAlertDialog("Please select Image to continue!")
            else if (OpenGallery.imagesSelected.size < maxCount && !isNext) {
                isNext = true
                ShowAlertDialog("You haven't selected all images!")
            } else {

                if (!intent.getBooleanExtra("isCover", false)) {
                    selectedImages = ArrayList()
                    for (img in OpenGallery.imagesSelected) selectedImages.add(SelectedImagesData(
                        img,
                        false))
                    startActivity(
                        Intent(this, SelectedImagesActivity::class.java)
                            .putExtra("size", intent.getStringExtra("size"))
                            .putExtra(
                                "msg",
                                "Please crop/rotate all images to fill frame completely"
                            )
                            .putExtra("isCropped", false)
                            .putExtra("isCover", false)
                    )
                } else {
                    selectedCover = ArrayList()
                    // for (img in OpenGallery.imagesSelected) selectedCover.add(File(img))
                    for (img in OpenGallery.imagesSelected) selectedCover.add(SelectedImagesData(img,
                        false))
                    startActivity(Intent(this, SelectedImagesActivity::class.java)
                        .putExtra("size", intent.getStringExtra("size"))
                        .putExtra(
                            "msg",
                            "Please crop/rotate all images to fill frame completely"
                        )
                        .putExtra("isCropped", false)
                        .putExtra("isCover", true)
                    )
                }
                finish()
            }
        }
    }
}
