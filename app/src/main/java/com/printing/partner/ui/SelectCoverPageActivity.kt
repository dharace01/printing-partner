package com.printing.partner.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.printing.partner.R
import com.printing.partner.adapter.CoverImageAdapter
import com.printing.partner.base.BaseActivity
import com.printing.partner.pref.AppPref
import com.printing.partner.ui.PhotosSelectionActivity.Companion.selectedCover
import com.printing.partner.ui.PhotosSelectionActivity.Companion.selectedCoverUrl
import com.printing.partner.util.IsPermissionsAllowedCameraAndStorage
import com.printing.partner.util.RequestPermissionCameraAndStorage
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_select_cover_page.*
import kotlinx.android.synthetic.main.activity_select_cover_page.ivBack
import java.io.File
import java.util.*
import kotlin.collections.ArrayList
import com.printing.partner.ui.DashboardActivity.Companion as DashboardActivity

class SelectCoverPageActivity : BaseActivity(R.layout.activity_select_cover_page),
    View.OnClickListener {

    private var coverImageAdapter: CoverImageAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {

        for(item in DashboardActivity.selData?.cover!!){
            item.isSelected = false
        }

        recycler_view.layoutManager = GridLayoutManager(this, 2)
       // DashboardActivity.selData!!.cover!![0].isSelected = true
        coverImageAdapter = CoverImageAdapter().also {
            if (DashboardActivity.selData!!.cover != null) {
                it.addAll(DashboardActivity.selData!!.cover)
            } else {
                it.addAll(arrayListOf())
            }

            it.setItemClickListener { view, i, typeData ->
                when (view.id) {
                    R.id.layout -> {

                        for (coverdata in DashboardActivity.selData!!.cover!!) {
                            coverdata.isSelected = coverdata == typeData
                        }
                        coverImageAdapter!!.notifyDataSetChanged()

                        if (i == 0) {
                            if (IsPermissionsAllowedCameraAndStorage()) {
                                startActivity(Intent(this@SelectCoverPageActivity, PhotosSelectionActivity::class.java)
                                        .putExtra("count", 2)
                                        .putExtra("size", intent.getStringExtra("size"))
                                        .putExtra("isCover", true)
                                )
                            } else {
                                RequestPermissionCameraAndStorage()
                            }
                        }

                    }
                }
            }
        }
        recycler_view.adapter = coverImageAdapter
    }

    private fun initOnClick() {
        ivBack.setOnClickListener(this)
        tvUpload.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> {
                onBackPressed()
            }
            R.id.tvUpload -> {
                for ((i, item) in DashboardActivity.selData!!.cover!!.withIndex()) {
                    if (item.isSelected!!) {
                        DashboardActivity.selCoverPrice = item.price!!
                        if (i == 0) {
                            if (selectedCover.size > 0) {
                                selectedCoverUrl = 0
                                if (AppPref.IsLogin || AppPref.IsGuest) {
                                    AppPref.IsGuest = false
                                    startActivity(Intent(this, CheckoutActivity::class.java).putExtra("isCart", false))
                                    //finish()
                                }
                                else {
                                    sneakerError("Please login first to continue!")
                                    startActivity(Intent(this, LoginActivity::class.java))
                                }
                            } else {
                                sneakerError("Please select Cover Image to continue!")
                            }
                        } else {
                            selectedCover = ArrayList()
                            selectedCoverUrl = item.id!!
                            if (AppPref.IsLogin || AppPref.IsGuest) {
                                AppPref.IsGuest = false
                                startActivity(Intent(this, CheckoutActivity::class.java).putExtra("isCart", false))
                                finish()
                            }
                            else {
                                sneakerError("Please login first to continue!")
                                startActivity(Intent(this, LoginActivity::class.java))
                            }
                        }
                    }
                }
            }
        }
    }
}