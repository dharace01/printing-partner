package com.printing.partner.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.pdf.PdfRenderer
import android.net.Uri
import android.os.*
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.itextpdf.io.image.ImageData
import com.itextpdf.io.image.ImageDataFactory
import com.itextpdf.kernel.colors.ColorConstants
import com.itextpdf.kernel.geom.PageSize
import com.itextpdf.kernel.pdf.PdfDocument
import com.itextpdf.kernel.pdf.PdfWriter
import com.itextpdf.layout.Document
import com.itextpdf.layout.borders.Border
import com.itextpdf.layout.borders.SolidBorder
import com.itextpdf.layout.element.Cell
import com.itextpdf.layout.element.Image
import com.itextpdf.layout.element.Table
import com.itextpdf.layout.property.HorizontalAlignment
import com.itextpdf.layout.property.UnitValue
import com.itextpdf.layout.renderer.CellRenderer
import com.printing.partner.R
import com.printing.partner.adapter.SelectedImageAdapter
import com.printing.partner.base.BaseActivity
import com.printing.partner.model.SelectedImagesData
import com.printing.partner.pref.AppPref
import com.printing.partner.ui.PhotosSelectionActivity.Companion.maxCount
import com.printing.partner.ui.PhotosSelectionActivity.Companion.selectedImages
import com.printing.partner.util.HideDialog
import com.printing.partner.util.ShowAlertDialog
import com.printing.partner.util.ShowDialog
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageActivity
import com.theartofdev.edmodo.cropper.CropImageOptions
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_photos_selection.ivBack
import kotlinx.android.synthetic.main.activity_photos_selection.tvNext
import kotlinx.android.synthetic.main.activity_photos_selection.tvTitle
import kotlinx.android.synthetic.main.activity_selected_images.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

class SelectedImagesActivity : BaseActivity(R.layout.activity_selected_images),
    View.OnClickListener {

    companion object {
        @JvmField
        var finalImages: ArrayList<File> = ArrayList()
    }

    private var selectedIndex: Int = 0
    private var selectedImageAdapter: SelectedImageAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    var widthRatio: Double = 0.0
    var heightRatio: Double = 0.0

    private var pageWidth: Int = 0
    private var pageHeight: Int = 0
    private var imagePerPage: Int = 0
    private var bottomPadding: Double = 0.0
    private var borderWidth = 4F

    private fun initView() {

        ShowAlertDialog(intent.getStringExtra("msg").plus(""))
        tvTitle.text = PhotosSelectionActivity.title

        val size = intent.getStringExtra("size")
        val height: String = size!!.substringAfter(":")
        val width: String = size.substringBefore(":")

        when {
            DashboardActivity.selPlan!!.contains("Snaps") && size == "9:12" -> {
                widthRatio = height.toDouble()
                heightRatio = width.toDouble()
            }
            DashboardActivity.selPlanType1!!.contains("Landscape Frame") -> {
                widthRatio = height.toDouble()
                heightRatio = width.toDouble()
            }
            else -> {
                widthRatio = width.toDouble()
                heightRatio = height.toDouble()
            }
        }

        if (DashboardActivity.selPlan!!.contains("Frame")) when (size) {
            "4:6", "6:6", "6:9", "9:12", "13:19" -> {
                pageWidth = widthRatio.toInt()
                pageHeight = heightRatio.toInt()
                bottomPadding = 0.0
                borderWidth = 0F
                imagePerPage = 1
            }
            "12:18" -> {
                pageWidth = widthRatio.toInt()
                pageHeight = heightRatio.toInt()
                bottomPadding = 0.0
                borderWidth = 0F
                imagePerPage = 1
            }
        } else
            when (size) {
                "4:6", "6:9", "4:4.5", "9:12" -> {
                    pageWidth = 12
                    pageHeight = 18
                    bottomPadding = (6 / 72F).toDouble()
                    borderWidth = 4F
                    imagePerPage =
                        ((pageHeight * pageWidth) / (widthRatio * heightRatio)).roundToInt()
                }

                "6:6" -> {
                    pageWidth = 12
                    pageHeight = 18
                    bottomPadding = 0.00
                    borderWidth = 4F
                    imagePerPage = 6
                }

                "3:4" -> {
                    pageWidth = 18
                    pageHeight = 12
                    bottomPadding = (6 / 72F).toDouble()
                    borderWidth = 4F
                    imagePerPage =
                        ((pageHeight * pageWidth) / (widthRatio * heightRatio)).roundToInt()
                }
                "3:3" -> {
                    pageWidth = 18
                    pageHeight = 12
                    bottomPadding = 1.22
                    borderWidth = 8F
                    imagePerPage =
                        ((pageHeight * pageWidth) / (widthRatio * (heightRatio + 1.0))).roundToInt()
                }
                "4:4" -> {
                    pageWidth = 12
                    pageHeight = 18
                    bottomPadding = 0.8
                    borderWidth = 12F
                    imagePerPage =
                        ((pageHeight * pageWidth) / (widthRatio * (heightRatio + 0.5))).roundToInt()
                }
                "5.5:5.5" -> {
                    pageWidth = 11
                    pageHeight = 18
                    bottomPadding = 0.8
                    borderWidth = 12F
                    imagePerPage = 6
                }
            }

        //  if (!intent.getBooleanExtra("isCropped", false)) deleteCache(this)
        recycler_view.layoutManager = LinearLayoutManager(this)
        selectedImageAdapter =
            SelectedImageAdapter(size, intent.getBooleanExtra("isCropped", false)).also {
                if (!intent.getBooleanExtra("isCover", false)) {
                    it.addAll(selectedImages)
                } else {
                    it.addAll(PhotosSelectionActivity.selectedCover)
                }
                it.setItemClickListener { view, position, image ->
                    when (view.id) {
                        R.id.layout -> {
                            selectedIndex = position
                            val mOptions = CropImageOptions()
                            mOptions.cropShape = CropImageView.CropShape.RECTANGLE
                            mOptions.fixAspectRatio = true
                            mOptions.aspectRatioX = (widthRatio * 2).roundToInt()
                            mOptions.aspectRatioY = (heightRatio * 2).roundToInt()
                            mOptions.activityTitle = size

                            val intent = Intent()
                            intent.setClass(this, CropImageActivity::class.java)
                            val bundle = Bundle()
                            bundle.putParcelable(
                                CropImage.CROP_IMAGE_EXTRA_SOURCE,
                                Uri.fromFile(File(image.img))
                            )
                            bundle.putParcelable(CropImage.CROP_IMAGE_EXTRA_OPTIONS, mOptions)
                            intent.putExtra(CropImage.CROP_IMAGE_EXTRA_BUNDLE, bundle)
                            startActivityForResult(
                                intent,
                                CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE
                            )
                        }
                    }
                }
            }
        recycler_view.adapter = selectedImageAdapter
    }

    private fun initOnClick() {
        ivBack.setOnClickListener(this)
        tvNext.setOnClickListener(this)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> onBackPressed()
            R.id.tvNext -> {

                Thread {
                    Log.e("Cover", intent.getBooleanExtra("isCover", false).toString());
                    if (!intent.getBooleanExtra("isCover", false)) {
                        runOnUiThread {
                            ShowDialog("Uploading Images...")
                        }
                        val selectedUrls = ArrayList<String>()
                        for (item in selectedImages)
                            if (item.isCropped!!) selectedUrls.add("file://${item.img}")
                            else selectedUrls.add("file://${cropImage1(item.img.toString())}")
                        Handler(Looper.getMainLooper()).postDelayed({
                            if (selectedUrls.size % imagePerPage != 0) {
                                val selectedOutputPath = cropImage()
                                for (i in 0 until maxCount) {
                                    if (selectedUrls.size % imagePerPage == 0) break
                                    selectedUrls.add("file://${selectedOutputPath}")
                                }
                            }
                            createPdf(selectedUrls)
                        }, (selectedUrls.size * 300).toLong())
                    } else {
                        runOnUiThread { finish() }
                    }
                }.start()


            }
        }
    }

    private fun cropImage(): String {
        val b: Bitmap
        val d: Drawable? = ContextCompat.getDrawable(this, R.drawable.default_logo)
        val bitDw: BitmapDrawable = d as BitmapDrawable
        b = bitDw.bitmap
        val selectedOutputPath: String = cacheDir.absolutePath + File.separator + "default.jpg"

        val thread: Thread = object : Thread() {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun run() {

                val out: Bitmap
                val ratioX = widthRatio
                val ratioY = heightRatio
                val imgWidth: Double = b.width.toDouble()
                val imgHeight: Double = b.height.toDouble()

                when {
                    imgWidth / imgHeight > ratioX / ratioY -> {
                        val extraWidth = imgWidth - imgHeight * (ratioX / ratioY)
                        val cropStartFrom = extraWidth / 2
                        out = Bitmap.createBitmap(
                            b,
                            cropStartFrom.toInt(),
                            0,
                            (b.width - extraWidth).toInt(),
                            b.height
                        )
                    }

                    imgWidth / imgHeight < ratioX / ratioY -> {
                        val extraHeight =
                            if (imgHeight > imgWidth) imgHeight - (imgWidth * ratioY / ratioX) else imgHeight - imgWidth * ratioY / ratioX
                        val cropStartFrom = extraHeight / 2
                        out = Bitmap.createBitmap(b,
                            0,
                            cropStartFrom.toInt(),
                            b.width,
                            (b.height - extraHeight).toInt())
                    }
                    else -> {
                        out = b
                    }
                }

                val file = File(selectedOutputPath)
                val fOut: FileOutputStream
                try {
                    fOut = FileOutputStream(file)
                    out.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                    fOut.flush()
                    fOut.close()
                    b.recycle()
                    out.recycle()
                } catch (e: java.lang.Exception) {
                }
            }
        }
        thread.run()
        return selectedOutputPath
    }

    private fun cropImage1(s: String): String {

        val imgFile = File(s)
        val imageName = "resize_${imgFile.name}$.jpg"
        val selectedOutputPath = cacheDir.absolutePath + File.separator + imageName
        val b = getBitmapFilepath(imgFile.absolutePath)

        val thread: Thread = object : Thread() {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun run() {

                val out: Bitmap
                val ratioX = widthRatio
                val ratioY = heightRatio
                val imgWidth: Double = b.width.toDouble()
                val imgHeight: Double = b.height.toDouble()

                when {
                    imgWidth / imgHeight > ratioX / ratioY -> {
                        out = Bitmap.createBitmap(imgWidth.toInt(),
                            ((imgWidth * (ratioY / ratioX)).roundToInt()),
                            Bitmap.Config.RGB_565)
                        val canvas = Canvas(out)
                        val startY: Float = (((out.height - imgHeight) / 2).toFloat())
                        canvas.drawColor(Color.WHITE);
                        canvas.drawBitmap(b, 0F, startY, Paint());
                    }

                    imgWidth / imgHeight < ratioX / ratioY -> {
                        out = Bitmap.createBitmap(((imgHeight * (ratioX / ratioY)).roundToInt()),
                            imgHeight.toInt(),
                            Bitmap.Config.RGB_565)
                        val canvas = Canvas(out)
                        val startX: Float = (((out.width - imgWidth) / 2).toFloat())
                        canvas.drawColor(Color.WHITE);
                        canvas.drawBitmap(b, startX, 0F, Paint());
                    }
                    else -> {
                        out = b
                    }
                }

                val file = File(selectedOutputPath)
                val fOut: FileOutputStream
                try {
                    fOut = FileOutputStream(file)
                    out.compress(Bitmap.CompressFormat.JPEG, 50, fOut)
                    fOut.flush()
                    fOut.close()
                    b.recycle()
                    out.recycle()
                } catch (e: java.lang.Exception) {
                }
            }
        }
        thread.run()
        return selectedOutputPath
    }

    private fun getBitmapFilepath(absolutePath: String): Bitmap {
        return BitmapFactory.decodeFile(absolutePath)
    }

    @SuppressLint("SimpleDateFormat")
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun createPdf(selectedUrls: ArrayList<String>) {

        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val pdfName = "PDF_$timeStamp.pdf"
        val pdfPath = cacheDir.absolutePath + File.separator + pdfName

        val width: Float = pageWidth * 72F
        val height: Float = pageHeight * 72F

        val pdfDoc = PdfDocument(PdfWriter(pdfPath))
        val pageSize = PageSize(width, height)
        val doc = Document(pdfDoc, pageSize)
        doc.setMargins(0f, 0f, 0f, 0f)

        val column: Int = (pageWidth / widthRatio).roundToInt()
        val table = Table(UnitValue.createPercentArray(column))
        //    table.useAllAvailableWidth()
        //   table.setExtendBottomRow(false)
        table.setBackgroundColor(ColorConstants.BLUE)

        for (item in selectedUrls) {
            Log.e("item", item + "----" + File(item).length());
            table.addCell(createImageCell(item))
        }
        doc.add(table)
        doc.close()
        pdfToBitmap(File(pdfPath))
    }

    private fun createImageCell(path: String?): Cell? {

        val imageWidth: Float = ((widthRatio * 72F) - borderWidth * 2.01).toFloat()
        val imageHeight: Float = ((heightRatio * 72F) - borderWidth * 2.01).toFloat()

        val imageData: ImageData = ImageDataFactory.create(path)
        val image = Image(imageData)
        image.scaleToFit(imageWidth, imageHeight)
        image.setBorder(SolidBorder(ColorConstants.WHITE, borderWidth))
        image.setHorizontalAlignment(HorizontalAlignment.CENTER)

        val cell: Cell = Cell().add(image.setAutoScaleHeight(true))
        cell.setBackgroundColor(ColorConstants.WHITE)
        cell.setPadding(0F)
        cell.setBorder(Border.NO_BORDER)
        cell.setMargin(0F)
        cell.setWidth(imageWidth)
        cell.setHeight(imageHeight + (bottomPadding.toFloat() * 72F))
        cell.setNextRenderer(CellRenderer(cell))
        cell.isKeepTogether = true

        return cell
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun pdfToBitmap(pdfFile: File) {

        Thread {
            finalImages = ArrayList()
            val resolution = 300
            val width: Int = pageWidth * resolution
            val height: Int = pageHeight * resolution

            try {
                val renderer = PdfRenderer(ParcelFileDescriptor.open(pdfFile,
                    ParcelFileDescriptor.MODE_READ_ONLY))
                var bitmap: Bitmap
                val pageCount: Int = renderer.pageCount

                for (i in 0 until pageCount) {
                    val page: PdfRenderer.Page = renderer.openPage(i)
                    bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
                    page.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY)
                    // close the page
                    page.close()

                    val paint = Paint()
                    paint.color = Color.WHITE
                    val newBitmap = Bitmap.createBitmap(bitmap.width,
                        bitmap.height,
                        bitmap.config)
                    val canvas = Canvas(newBitmap)
                    canvas.drawRect(0F,
                        0F,
                        bitmap.width.toFloat(),
                        bitmap.height.toFloat(),
                        paint)
                    canvas.drawBitmap(bitmap, 0F, 0F, paint)

                    val imageByteArray = ByteArrayOutputStream()
                    newBitmap.compress(Bitmap.CompressFormat.JPEG, 50, imageByteArray)
                    val imageData: ByteArray = imageByteArray.toByteArray()

                    setDpi(imageData, 300)

                    val imgName = "${AppPref.userName}${i + 1}.jpg"
                    val imgPath = cacheDir.absolutePath + File.separator + imgName

                    val file = File(imgPath)
                    val fOut: FileOutputStream
                    try {
                        fOut = FileOutputStream(file)
                        fOut.write(imageData)
                        fOut.flush()
                        fOut.close()
                    } catch (e: java.lang.Exception) {
                    }
                    finalImages.add(File(imgPath))
                }
                // close the renderer
                renderer.close()

                runOnUiThread {
                    HideDialog()
                    PhotosSelectionActivity.selectedCoverUrl = 0
                    when {
                        DashboardActivity.selPlanType == "Ocassional Frame" -> startActivity(Intent(
                            this,
                            DetailsActivity::class.java))
                        DashboardActivity.selData!!.cover == null -> if (AppPref.IsLogin) startActivity(
                            Intent(
                                this,
                                CheckoutActivity::class.java).putExtra("isCart", false))
                        else {
                            sneakerError("Please login first to continue!")
                            startActivity(Intent(this, LoginActivity::class.java))
                        }
                        else -> startActivity(Intent(this,
                            SelectCoverPageActivity::class.java).putExtra("size",
                            intent.getStringExtra(
                                "size")))
                    }
                    //  finish()
                }

            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }.start()
    }

    private fun setDpi(imageData: ByteArray, dpi: Int) {
        imageData[13] = 1
        imageData[14] = (dpi shr 8).toByte()
        imageData[15] = (dpi and 0xff).toByte()
        imageData[16] = (dpi shr 8).toByte()
        imageData[17] = (dpi and 0xff).toByte()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == RESULT_OK) {

                if (!intent.getBooleanExtra("isCover", false)) {
                    selectedImages[selectedIndex] = SelectedImagesData(result.uri.path.toString(),
                        true)
                } else {
                    PhotosSelectionActivity.selectedCover[selectedIndex] =
                        SelectedImagesData(result.uri.path.toString(),
                            true)
                }
                selectedImageAdapter!!.replaceItemAt(selectedIndex,
                    SelectedImagesData(result.uri.path.toString(),
                        true)
                )
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                sneakerError("Cropping failed: " + result.error)
            }
        }
    }
}
