package com.printing.partner.ui

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.printing.partner.R
import com.printing.partner.base.BaseActivity
import com.printing.partner.pref.AppPref
import com.printing.partner.util.isValidEmail
import com.printing.partner.util.isValidPhoneNumber
import kotlinx.android.synthetic.main.activity_guest.*
import kotlinx.android.synthetic.main.activity_login.edUsername
import kotlinx.android.synthetic.main.activity_login.txtPassword
import kotlinx.android.synthetic.main.activity_order_details.ivBack
import kotlinx.android.synthetic.main.activity_register.edCity
import kotlinx.android.synthetic.main.activity_register.edCode
import kotlinx.android.synthetic.main.activity_register.edEmail
import kotlinx.android.synthetic.main.activity_register.edFname
import kotlinx.android.synthetic.main.activity_register.edLname
import kotlinx.android.synthetic.main.activity_register.edMobile
import kotlinx.android.synthetic.main.activity_register.edShippingAdd

class GuestActivity : BaseActivity(R.layout.activity_guest), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        chkAddress.setOnCheckedChangeListener { buttonView, isChecked ->
            llBillingAddress.visibility = if (!isChecked) View.VISIBLE else View.GONE
        }
    }

    private fun initOnClick() {
        ivBack.setOnClickListener(this)
        btnProceed.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> onBackPressed()
            R.id.btnProceed -> {
                if (isValidate()) {
                    setRegister()
                }
            }
        }
    }

    private fun isValidate(): Boolean {
        return when {

            TextUtils.isEmpty(edFname.text.toString().trim()) -> {
                edFname.error = getString(R.string.please_enter_your_first_name)
                false
            }
            TextUtils.isEmpty(edLname.text.toString().trim()) -> {
                edLname.error = getString(R.string.please_enter_your_last_name)
                false
            }
            !isValidPhoneNumber(edMobile.text.toString().trim()) -> {
                edMobile.error = getString(R.string.please_enter_your_mobile_number)
                false
            }
            !isValidEmail(edEmail.text.toString().trim()) -> {
                edEmail.error = getString(R.string.please_enter_valid_email_address)
                false
            }
            TextUtils.isEmpty(edShippingAdd.text.toString().trim()) -> {
                edShippingAdd.error = getString(R.string.please_enter_your_shipping_address)
                false
            }
            TextUtils.isEmpty(edCity.text.toString().trim()) -> {
                edCity.error = getString(R.string.please_enter_your_city)
                false
            }
            TextUtils.isEmpty(edCode.text.toString().trim()) -> {
                edCode.error = getString(R.string.please_enter_your_pincode)
                false
            }

            TextUtils.isEmpty(edBillingAdd.text.toString().trim()) && !chkAddress.isChecked -> {
                edBillingAdd.error = getString(R.string.please_enter_your_shipping_address)
                false
            }

            TextUtils.isEmpty(edBillingCity.text.toString().trim()) && !chkAddress.isChecked -> {
                edBillingCity.error = getString(R.string.please_enter_your_city)
                false
            }
            TextUtils.isEmpty(edBillingCode.text.toString().trim()) && !chkAddress.isChecked -> {
                edBillingCode.error = getString(R.string.please_enter_your_pincode)
                false
            }

            else -> {
                true
            }
        }
    }

    private fun setRegister() {
        val prm = HashMap<String, String>()
        prm["fname"] = edFname.text.toString().trim()
        prm["lname"] = edLname.text.toString().trim()
        prm["mobile"] = edMobile.text.toString().trim()
        prm["email"] = edEmail.text.toString().trim()
        prm["password"] = ""
        prm["address"] = edShippingAdd.text.toString().trim()
        prm["city"] = edCity.text.toString().trim()
        prm["pincode"] = edCode.text.toString().trim()
        prm["username"] = ""
        prm["mob_check"] = "0"
        prm["email_check"] = "0"
        prm["is_register"] = "0"
        prm["is_guest"] = "1"
        prm["billing_add"] = if(chkAddress.isChecked) edShippingAdd.text.toString().trim() else edBillingAdd.text.toString().trim()
        prm["billing_city"] =  if(chkAddress.isChecked) edCity.text.toString().trim() else edBillingCity.text.toString().trim()
        prm["billing_pincode"] =  if(chkAddress.isChecked) edCode.text.toString().trim() else edBillingCode.text.toString().trim()

        callApi(apiClient.postRegistration(prm), true) { registrationResponse ->
            if (registrationResponse.status!!) {
                AppPref.IsGuest = true
                AppPref.userID = registrationResponse.data?.unique_id.toString()
                AppPref.userName = """${registrationResponse.data?.fname} ${registrationResponse.data?.lname}"""
                finish()
            } else {
                sneakerError(registrationResponse.msg!!)
            }
        }
    }
}