package com.printing.partner.ui;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.printing.partner.R;
import com.printing.partner.adapter.MediaAdapter;
import com.printing.partner.fragments.GalleryFragment;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class OpenGallery extends AppCompatActivity {

    public static List<Boolean> selected = new ArrayList<>();
    public static ArrayList<String> imagesSelected = new ArrayList<>();
    private RecyclerView recyclerView;
    private ImageView ivBack;
    private TextView tvTitle;
    private MediaAdapter mAdapter;
    private List<String> mediaList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_gallery);

        FloatingActionButton fab = findViewById(R.id.fab);
        ivBack = findViewById(R.id.ivBack);
        tvTitle = findViewById(R.id.tvTitle);

        fab.setOnClickListener(view -> finish());
        ivBack.setOnClickListener(v -> onBackPressed());

        PhotosSelectionActivity.title = imagesSelected.size() + "/" + PhotosSelectionActivity.maxCount;
        tvTitle.setText(PhotosSelectionActivity.title);

        recyclerView = findViewById(R.id.recycler_view);
        mediaList.clear();
        selected.clear();

        imagesSelected.clear();
        mediaList.addAll(GalleryFragment.imagesList);
        selected.addAll(GalleryFragment.selected);

        populateRecyclerView();
    }

    Toast toast;

    private void populateRecyclerView() {

        for (int i = 0; i < selected.size(); i++)
            if (imagesSelected.contains(mediaList.get(i))) selected.set(i, true);
            else selected.set(i, false);

        mAdapter = new MediaAdapter(mediaList, selected, getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.getItemAnimator().setChangeDuration(0);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new ClickListener() {

            @Override
            public void onClick(View view, int position) {

                if(toast!=null){
                    toast.cancel();
                }

                if (!selected.get(position).equals(true) && imagesSelected.size() < PhotosSelectionActivity.maxCount) {
                    if(!selected.get(position)){
                        File file = new File(mediaList.get(position));
                        int file_size = Integer.parseInt(String.valueOf(file.length() / 1024));
                        if (file_size < 50){
                            toast = Toast.makeText(OpenGallery.this, "Resolution of this image is low!", Toast.LENGTH_SHORT);
                            toast.show();
                            Log.e("TAG", "Low resolution image");
                        }else{
                            Log.e("TAG", "High resolution");
                        }
                    }
                    imagesSelected.add(mediaList.get(position));
                    selected.set(position, !selected.get(position));
                    mAdapter.notifyItemChanged(position);
                } else if (selected.get(position).equals(true)) {
                    if (imagesSelected.indexOf(mediaList.get(position)) != -1) {
                        imagesSelected.remove(mediaList.get(position));
                        selected.set(position, !selected.get(position));
                        mAdapter.notifyItemChanged(position);
                    }
                }
                PhotosSelectionActivity.title = imagesSelected.size() + "/" + PhotosSelectionActivity.maxCount;
                tvTitle.setText(PhotosSelectionActivity.title);
            }

            @Override
            public void onLongClick(View view, int position) {}
        }));
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {
        private GestureDetector gestureDetector;
        private OpenGallery.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final OpenGallery.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(@NotNull RecyclerView rv, @NotNull MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }
    }
}

