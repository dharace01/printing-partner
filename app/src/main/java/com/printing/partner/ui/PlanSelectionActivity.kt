package com.printing.partner.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.printing.partner.R
import com.printing.partner.adapter.PlanSelectionAdapter
import com.printing.partner.base.BaseActivity
import com.printing.partner.model.TypeData
import com.printing.partner.util.IsPermissionsAllowedCameraAndStorage
import com.printing.partner.util.RequestPermissionCameraAndStorage
import com.printing.partner.util.setUpSlider
import kotlinx.android.synthetic.main.activity_plan_selection.*

class PlanSelectionActivity : BaseActivity(R.layout.activity_plan_selection), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        val list: ArrayList<TypeData> = intent.getParcelableArrayListExtra("list")!!
        tvTitle.text = intent.getStringExtra("title")
        tvName.text = DashboardActivity.selPlan
        setUpSlider(DashboardActivity.selData!!.slider, viewPager)
        tvDetails.text = DashboardActivity.selData!!.description
        DashboardActivity.shipping_charge = DashboardActivity.selData!!.shipping_charge!!
        setData(list)
    }

    override fun onResume() {
        super.onResume()
        PhotosSelectionActivity.selectedImages = ArrayList()
        PhotosSelectionActivity.selectedCover = ArrayList()
    }

    private fun setData(list: ArrayList<TypeData>?) {

        rvList.layoutManager = LinearLayoutManager(this)
        rvList.run {
            adapter = PlanSelectionAdapter().also {

                if (list != null) it.addAll(list) else it.addAll(emptyList())

                it.setItemClickListener { view, _, typeData ->
                    when (view.id) {
                        R.id.btnSelect, R.id.layout -> {
                            if (typeData.t_id != 0) {
                                DashboardActivity.type_name = typeData.name.toString()
                                DashboardActivity.type_id = typeData.t_id!!
                            } else DashboardActivity.type_id = DashboardActivity.type_id


                            if (typeData.st_id != 0) {
                                DashboardActivity.subtype_id = typeData.st_id!!
                                DashboardActivity.subtype_name = typeData.name.toString()
                            } else DashboardActivity.subtype_id = DashboardActivity.subtype_id

                            if (typeData.sst_id != 0) {
                                DashboardActivity.subsubtype_id = typeData.sst_id!!
                                DashboardActivity.subsubtype_name = typeData.name.toString()
                            } else DashboardActivity.subsubtype_id = DashboardActivity.subsubtype_id

                            when {
                                typeData.subtype != null && typeData.subtype.size > 0 -> {
                                    DashboardActivity.selPlanType1 = ""
                                    DashboardActivity.selPlanType1 = typeData.name
                                    startActivity(Intent(this@PlanSelectionActivity,
                                        PlanSelectionActivity::class.java)
                                        .putExtra("title", typeData.name)
                                        .putExtra("list", typeData.subtype)
                                    )
                                }

                                typeData.subtype1 != null && typeData.subtype1.size > 0 -> {
                                    DashboardActivity.selPlanType2 = ""
                                    DashboardActivity.selPlanType2 = typeData.name
                                    startActivity(Intent(this@PlanSelectionActivity,
                                        PlanSelectionActivity::class.java).putExtra("title",
                                        typeData.name).putExtra("list", typeData.subtype1))
                                }

                                else -> {
                                    DashboardActivity.selPlanType = ""
                                    DashboardActivity.selPlanType = typeData.name

                                    if (intent.getStringExtra("title") == "Snapbook") {
                                        when (typeData.name) {
                                            "Regular" -> {
                                                DashboardActivity.maxCount = 36
                                            }
                                            "Classic Large" -> {
                                                DashboardActivity.maxCount = 24
                                            }
                                            else -> {
                                                DashboardActivity.maxCount = 16
                                            }
                                        }
                                    } else {
                                        DashboardActivity.maxCount =
                                            DashboardActivity.selData!!.count ?: typeData.count
                                    }

                                    DashboardActivity.selSize = typeData.size
                                    DashboardActivity.selPrice = typeData.price!!
                                    var size = typeData.size!!.replace("x", ":")
                                    size = size.subSequence(0, size.indexOf(" ")) as String
                                    Log.e("size", size + DashboardActivity.maxCount)
                                    if (IsPermissionsAllowedCameraAndStorage()) {
                                        startActivity(
                                            Intent(this@PlanSelectionActivity,
                                                PhotosSelectionActivity::class.java)
                                                .putExtra("count", DashboardActivity.maxCount)
                                                .putExtra("size", size).putExtra("isCover", false)
                                        )
                                    } else {
                                        RequestPermissionCameraAndStorage()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun initOnClick() {
        ivBack.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> {
                onBackPressed()
            }
        }
    }
}