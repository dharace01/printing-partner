package com.printing.partner.ui

import android.content.*
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.printing.partner.R
import com.printing.partner.base.BaseActivity
import com.printing.partner.model.ProfileData
import com.printing.partner.pref.AppPref
import com.printing.partner.util.isValidEmail
import com.printing.partner.util.isValidPhoneNumber
import kotlinx.android.synthetic.main.activity_my_account.*


class MyAccountActivity : BaseActivity(R.layout.activity_my_account), View.OnClickListener {

    private var isCopied: Boolean? = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        callGetProfile()
    }

    private fun initOnClick() {
        btnConfirm.setOnClickListener(this)
        btnCopy.setOnClickListener(this)
        ivInsta.setOnClickListener(this)
        ivFB.setOnClickListener(this)
        ivWP.setOnClickListener(this)
        ivBack.setOnClickListener(this)
        ivLogout.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> {
                onBackPressed()
            }
            R.id.ivLogout -> {
                AppPref.clearPref()
                finish()
            }
            R.id.btnConfirm -> {
                if (isValidate()) {
                    callEditProfile()
                }
            }
            R.id.btnCopy -> {
                val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clip: ClipData
                if (isCopied!!) {
                    btnCopy.text = "Copy"
                    clip = ClipData.newPlainText("", "")
                } else {
                    btnCopy.text = "Copied"
                    clip = ClipData.newPlainText("link", edLink.text.toString())
                }
                clipboard.setPrimaryClip(clip)
                isCopied = !isCopied!!
            }
            R.id.ivInsta -> {
                shareLink("com.instagram.android", "Instagram")
            }
            R.id.ivFB -> {
                shareLink("com.facebook.orca", "Facebook Messenger")
            }
            R.id.ivWP -> {
                shareLink("com.whatsapp", "Whatsapp")
            }
        }
    }

    private fun shareLink(packageName: String, appName: String) {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        shareIntent.setPackage(packageName)
        shareIntent.putExtra(Intent.EXTRA_TEXT, edLink.text.toString())
        try {
            startActivity(shareIntent)
        } catch (ex: ActivityNotFoundException) {
            sneakerError("Please install $appName to share link!")
        }
    }

    private fun isValidate(): Boolean {
        return when {
            TextUtils.isEmpty(edFname.text.toString().trim()) -> {
                edFname.error = getString(R.string.please_enter_your_first_name)
                false
            }

            TextUtils.isEmpty(edLname.text.toString().trim()) -> {
                edLname.error = getString(R.string.please_enter_your_last_name)
                false
            }
            !isValidPhoneNumber(edMobile.text.toString().trim()) -> {
                edMobile.error = getString(R.string.please_enter_your_mobile_number)
                false
            }
            !isValidEmail(edEmail.text.toString().trim()) -> {
                edEmail.error = getString(R.string.please_enter_valid_email_address)
                false
            }
            TextUtils.isEmpty(edShippingAdd.text.toString().trim()) -> {
                edShippingAdd.error = getString(R.string.please_enter_your_shipping_address)
                false
            }
            TextUtils.isEmpty(edCity.text.toString().trim()) -> {
                edCity.error = getString(R.string.please_enter_your_city)
                false
            }
            TextUtils.isEmpty(edCode.text.toString().trim()) -> {
                edCode.error = getString(R.string.please_enter_your_pincode)
                false
            }
            else -> {
                true
            }
        }
    }

    private fun callGetProfile() {
        val prm = HashMap<String, String>()
        prm["unique_id"] = AppPref.userID
        callApi(apiClient.postGetProfile(prm), true) { profileResponse ->
            if (profileResponse.status!!) {
                setAccountData(profileResponse.data!!)
            } else {
                sneakerError(profileResponse.msg!!)
                AppPref.clearPref()
                finish()
            }
        }
    }

    private fun callEditProfile() {
        val prm = HashMap<String, String>()
        prm["unique_id"] = AppPref.userID
        prm["fname"] = edFname.text.toString().trim()
        prm["lname"] = edLname.text.toString().trim()
        prm["mobile"] = edMobile.text.toString().trim()
        prm["email"] = edEmail.text.toString().trim()
        prm["address"] = edShippingAdd.text.toString().trim()
        prm["city"] = edCity.text.toString().trim()
        prm["pincode"] = edCode.text.toString().trim()

        callApi(apiClient.postEditProfile(prm), true) { profileResponse ->
            if (profileResponse.status!!) {
                setAccountData(profileResponse.data!!)
            }
        }
    }

    private fun setAccountData(data: ProfileData) {
        edFname.setText(data.fname)
        edLname.setText(data.lname)
        edMobile.setText(data.mobile)
        edEmail.setText(data.email)
        edShippingAdd.setText(data.address)
        edCity.setText(data.city)
        edCode.setText(data.pincode)
        edLink.setText(data.link)
    }
}