package com.printing.partner.ui

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.printing.partner.R
import com.printing.partner.base.BaseActivity
import com.printing.partner.pref.AppPref
import kotlinx.android.synthetic.main.activity_verification.*

class VerificationActivity : BaseActivity(R.layout.activity_verification), View.OnClickListener {

    var mob_otp = ""
    var email_otp = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        mob_otp = intent.getStringExtra("mob_otp").toString()
        email_otp = intent.getStringExtra("email_otp").toString()
    }

    private fun initOnClick() {
        btnVerify.setOnClickListener(this)
        ivBack.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> onBackPressed()
            R.id.btnVerify -> if (isValidate()) setRegister()
        }
    }

    private fun setRegister() {
        val prm = HashMap<String, String>()
        prm["fname"] = intent.getStringExtra("fname").toString()
        prm["lname"] = intent.getStringExtra("lname").toString()
        prm["mobile"] = intent.getStringExtra("mobile").toString()
        prm["email"] = intent.getStringExtra("email").toString()
        prm["password"] = intent.getStringExtra("password").toString()
        prm["address"] = intent.getStringExtra("address").toString()
        prm["city"] = intent.getStringExtra("city").toString()
        prm["pincode"] = intent.getStringExtra("pincode").toString()
        prm["username"] = intent.getStringExtra("username").toString()
         prm["mob_check"] = "0"
        prm["email_check"] = "0"
        prm["is_register"] = "1"
        prm["is_guest"] = "0"
        callApi(apiClient.postRegistration(prm), true) { registrationResponse ->
            if (registrationResponse.status!!) {
                AppPref.IsLogin = true
                AppPref.userID = registrationResponse.data?.unique_id.toString()
                AppPref.userName = """${registrationResponse.data?.fname} ${registrationResponse.data?.lname}"""
                startActivity(Intent(this, MyAccountActivity::class.java))
                finish()
            } else {
                sneakerError(registrationResponse.msg!!)
            }
        }
    }

    private fun isValidate(): Boolean {
        return when {
            TextUtils.isEmpty(edCode.text.toString().trim()) -> {
                edCode.error = "Please enter verification code!"
                false
            }
           // edCode.text.toString() != mob_otp ||

            edCode.text.toString() != email_otp -> {
                sneakerError("Verification code is not valid!")
                false
            }
            else -> {
                true
            }
        }
    }
}