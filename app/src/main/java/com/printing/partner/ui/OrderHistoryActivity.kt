package com.printing.partner.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.printing.partner.R
import com.printing.partner.adapter.OrderHistoryAdapter
import com.printing.partner.base.BaseActivity
import com.printing.partner.model.OrderHistoryData
import com.printing.partner.pref.AppPref
import kotlinx.android.synthetic.main.activity_order_history.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set

class OrderHistoryActivity : BaseActivity(R.layout.activity_order_history), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        callOrderHistory()
    }

    private fun initOnClick() {
        ivBack.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> onBackPressed()
        }
    }

    private fun callOrderHistory() {
        val prm = HashMap<String, String>()
        prm["unique_id"] = AppPref.userID
        callApi(apiClient.getOrderistory(prm), true) { it ->
            rvOrders.layoutManager = LinearLayoutManager(this)
            val list = it.data
            llMain.visibility = View.VISIBLE
            rvOrders.adapter = OrderHistoryAdapter().also {
                if (list != null) it.addAll(list) else ArrayList<OrderHistoryData>();
                it.setItemClickListener { view, _, typeData ->
                    when (view.id) {
                          R.id.btnOrderDetails -> startActivity(
                              Intent(this, OrderDetailsActivity::class.java)
                                  .putExtra("data", typeData)
                          )
                    }
                }
            }
        }
    }
}