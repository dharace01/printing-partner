package com.printing.partner.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import com.printing.partner.R
import com.printing.partner.base.BaseActivity
import com.printing.partner.util.HideDialog
import com.printing.partner.util.ShowDialog
import kotlinx.android.synthetic.main.activity_faq.*


class FaqActivity : BaseActivity(R.layout.activity_faq), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initView() {

        ShowDialog("")
        webview.webViewClient = WebViewClient()
        webview.loadUrl(intent.getStringExtra("url").toString())
        webview.settings.javaScriptEnabled = true
        webview.settings.setSupportZoom(true)
        webview.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                if (progress == 100) {
                   HideDialog()
                }
            }
        }
    }

    private fun initOnClick() {
        ivBack.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> {
                onBackPressed()
            }
        }
    }

    override fun onBackPressed() {
        if (webview.canGoBack())
            webview.goBack()
        else
            super.onBackPressed()
    }

}

