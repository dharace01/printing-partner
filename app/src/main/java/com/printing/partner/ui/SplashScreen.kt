package com.printing.partner.ui

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.printing.partner.R
import com.printing.partner.base.BaseActivity
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask

class SplashScreen : BaseActivity(R.layout.activity_splash_screen) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(intentFor<DashboardActivity>().clearTask().newTask())
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }, 3000)
    }
}