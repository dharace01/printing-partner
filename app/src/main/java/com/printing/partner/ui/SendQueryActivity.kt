package com.printing.partner.ui

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.printing.partner.R
import com.printing.partner.base.BaseActivity
import com.printing.partner.util.isValidEmail
import kotlinx.android.synthetic.main.activity_contact_us.ivBack
import kotlinx.android.synthetic.main.activity_send_query.*

class SendQueryActivity : BaseActivity(R.layout.activity_send_query), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initOnClick()
    }

    private fun initOnClick() {
        ivBack.setOnClickListener(this)
        btnSend.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> onBackPressed()
            R.id.btnSend -> if (isValidate()) sendQuery()
        }
    }

    private fun isValidate(): Boolean {
        return when {
            !isValidEmail(edEmail.text.toString().trim()) -> {
                edEmail.error = getString(R.string.please_enter_valid_email_address)
                false
            }
            TextUtils.isEmpty(edMessage.text.toString().trim()) -> {
                edMessage.error = "Please enter message!"
                false
            }
            edMessage.text.toString().trim().length < 5 -> {
                edMessage.error = "Message must be atleast 15 characters long!"
                false
            }
            else -> {
                true
            }
        }
    }

    private fun sendQuery() {
        val prm = HashMap<String, String>()
        prm["email"] = edEmail.text.toString().trim()
        prm["message"] = edMessage.text.toString().trim()
        callApi(apiClient.postSendQuery(prm), true) {
            sneakerError(it.msg!!)
            if (it.status!!) {
                finish()
            }
        }
    }
}