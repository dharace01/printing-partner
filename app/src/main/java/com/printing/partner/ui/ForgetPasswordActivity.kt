package com.printing.partner.ui

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.printing.partner.R
import com.printing.partner.base.BaseActivity
import com.printing.partner.util.isValidEmail
import kotlinx.android.synthetic.main.activity_forget_password.*

class ForgetPasswordActivity : BaseActivity(R.layout.activity_forget_password),

    View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        btnVerify.visibility = View.VISIBLE
        btnProceed.visibility = View.GONE
        edCode.visibility = View.GONE
        edEmail.visibility = View.VISIBLE
        txtPassword.visibility = View.GONE
        txtConfirmPassword.visibility = View.GONE
    }

    private fun initOnClick() {
        btnProceed.setOnClickListener(this)
        btnVerify.setOnClickListener(this)
        ivBack.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> onBackPressed()
            R.id.btnVerify ->  if(isValidate()) callSendOTP()
            R.id.btnProceed -> if(isValidate1()) setForgetPass()
        }
    }

    var otp = "";
    private fun setForgetPassVerification() {
        btnVerify.visibility = View.GONE
        btnProceed.visibility = View.VISIBLE
        edCode.visibility = View.VISIBLE
        txtPassword.visibility = View.VISIBLE
        txtConfirmPassword.visibility = View.VISIBLE
        edCode.requestFocus()
        edEmail.visibility = View.GONE
    }

    private fun callSendOTP() {
        val prm = HashMap<String, String>()
        prm["email"] = edEmail.text.toString().trim()
        callApi(apiClient.postForgetPasswordVerify(prm), true) { forgetPasswordResponse ->
            if (forgetPasswordResponse.status!!) {
                otp = forgetPasswordResponse.otp.toString();
               setForgetPassVerification()
            }
            sneakerError(forgetPasswordResponse.msg!!)
        }
    }

    private fun isValidate(): Boolean {
        return when {
            !isValidEmail(edEmail.text.toString().trim()) -> {
                edEmail.error = getString(R.string.please_enter_valid_email_address)
                false
            }
            else -> {
                true
            }
        }
    }

    private fun isValidate1(): Boolean {
        return when {
            TextUtils.isEmpty(edCode.text.toString().trim()) -> {
                edCode.error = "Please enter verification code!"
                false
            }
            edCode.text.toString() != otp -> {
                sneakerError("Verification code is not valid!")
                false
            }
            TextUtils.isEmpty(txtPassword.text.toString().trim()) -> {
                txtPassword.error = getString(R.string.please_enter_your_password)
                false
            }
            TextUtils.isEmpty(txtConfirmPassword.text.toString().trim()) -> {
                txtConfirmPassword.error = getString(R.string.please_enter_confirm_password)
                false
            }
            txtPassword.text.toString() != txtConfirmPassword.text.toString() -> {
                txtConfirmPassword.error = getString(R.string.password_confirm_password_mismatch)
                false
            }
            else -> {
                true
            }
        }
    }

    private fun setForgetPass() {
        val prm = HashMap<String, String>()
        prm["email"] = edEmail.text.toString().trim()
        prm["new_password"] = txtPassword.text.toString().trim()
        callApi(apiClient.postForgetPassword(prm), true) { forgetPasswordResponse ->
            if (forgetPasswordResponse.status!!) {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
            sneakerError(forgetPasswordResponse.msg!!)
        }
    }

}